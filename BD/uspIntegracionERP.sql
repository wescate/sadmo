

ALTER PROCEDURE uspIntegracionERP
AS
BEGIN
--Inserta nuevos tipo doc Venta
insert into tbTipoDocVenta (CodigoTipoVenta,DescripcionTipoVenta)
select CodigoTipoVenta,DescripcionTipoVenta 
from migracion_tbTipoDocVenta tmp
where not exists (select '' from tbTipoDocVenta t where t.CodigoTipoVenta = tmp.CodigoTipoVenta )

--iNSERTA NUEVOS CLIENTES
INSERT INTO [dbo].[tbCliente]
           ([PrimerNombre]
           ,[SegundoNombre]
           ,[ApellidoPaterno]
           ,[ApellidoMaterno]
           ,[RazonSocial]
           ,[CodigoTipoDocIden]
           ,[NumeroDocIden]
           ,[Direccion]
           ,[CodigoUbigeo]
           ,[Telefono]
           ,[Correo]
           ,[Latitud]
           ,[Longitud])
  SELECT [PrimerNombre]
           ,[SegundoNombre]
           ,[ApellidoPaterno]
           ,[ApellidoMaterno]
           ,[RazonSocial]
           ,[CodigoTipoDocIden]
           ,[NumeroDocIden]
           ,[Direccion]
           ,[CodigoUbigeo]
           ,[Telefono]
           ,[Correo]
           ,[Latitud]
           ,[Longitud]
 FROM migracion_tbCliente TMP
 WHERE NOT EXISTS (SELECT '' FROM tbCliente T WHERE TMP.RazonSocial = T.RazonSocial)


 INSERT INTO tbVenta (
			CodigoTipoVenta,
			NumeroDocVenta,
			CodigoCliente,
			FechaEmision
			)
 SELECT 
 			CodigoTipoVenta,
			NumeroDocVenta,
			TC.CodigoCliente,
			FechaEmision
 FROM migracion_tbVenta tmp
 INNER JOIN migracion_tbCliente TMP_MC
 ON tmp.CodigoCliente = TMP_MC.CodigoCliente
 INNER JOIN tbCliente TC
 ON  TMP_MC.RazonSocial = TC.RazonSocial
 WHERE NOT  EXISTS (SELECT '' FROM tbVenta T WHERE TMP.NumeroDocVenta = T.NumeroDocVenta AND TMP.CodigoTipoVenta = T.CodigoTipoVenta AND TMP.CodigoCliente = T.CodigoCliente)
 
--INSERTA PRODUCTOS migrados
INSERT INTO [dbo].[tbProducto]
           ([DescripcionProducto]
           ,[CodigoUniMed]
           ,[MarcaProducto]
           ,[CodigoAlternativoProducto]
           ,[ObservacionProducto]
           ,[ProcesoCorteProducto])
SELECT		[DescripcionProducto]
           ,[CodigoUniMed]
           ,[MarcaProducto]
           ,[CodigoAlternativoProducto]
           ,[ObservacionProducto]
           ,[ProcesoCorteProducto]
FROM		migracion_tbProducto TMP
WHERE NOT EXISTS (SELECT '' FROM tbProducto T WHERE TMP.DescripcionProducto = T.DescripcionProducto)

--INSERTA UNIDAD DE MEDIDA
 
 INSERT INTO [dbo].[tbUniMed]
           ([CodigoUniMed]
           ,[DescripcionUniMed])
SELECT  [CodigoUniMed]
       ,[DescripcionUniMed]
FROM migracion_tbUniMed TMP
WHERE NOT EXISTS ( SELECT '' FROM tbUniMed T WHERE TMP.CodigoUniMed = T.CodigoUniMed )
 
 
 --INSERTA TABLA DETALLE VENTA
 
INSERT INTO [dbo].[tbDetalleVenta]
           ([CodigoTipoVenta]
           ,[NumeroDocVenta]
           ,[CodigoCliente]
           ,[CodigoProducto]
           ,[CantidadDetVenta])
 
SELECT      [CodigoTipoVenta]
           ,[NumeroDocVenta]
           ,TC.[CodigoCliente]
           ,[CodigoProducto]
           ,[CantidadDetVenta]
FROM       migracion_tbDetalleVenta TMP 
 INNER JOIN migracion_tbCliente TMP_MC
 ON tmp.CodigoCliente = TMP_MC.CodigoCliente
 INNER JOIN tbCliente TC
 ON  TMP_MC.RazonSocial = TC.RazonSocial
WHERE NOT EXISTS (SELECT '' FROM tbDetalleVenta T WHERE TMP.NumeroDocVenta = T.NumeroDocVenta AND TMP.CodigoTipoVenta = T.CodigoTipoVenta AND TMP.CodigoCliente = T.CodigoCliente )

DELETE from migracion_tbventa
DELETE from migracion_tbdetalleVenta
DELETE from migracion_tbproducto
DELETE from migracion_tbcliente
DELETE from migracion_tbUniMed
DELETE from migracion_tbTipoDocVenta
END
