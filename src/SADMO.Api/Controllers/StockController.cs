﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SADMO.Mantenimiento.Domain;
using SADMO.Mantenimiento.Infraestructure.Data.Interfaces;

namespace SADMO.Api.Controllers
{
    [Consumes("application/json")]
    [Produces("application/json")]
    [ProducesResponseType(200)]
    [ProducesResponseType(404)]
    [ProducesResponseType(500)]
    [EnableCors("AllowSpecificOrigin")] 
    [ApiController]
    public class StockController : ControllerBase
    {

        private readonly IStock stockRepository;

        public StockController(IStock stockRepository)
        {
            this.stockRepository = stockRepository;
        }

        [HttpPost("update")]
        public async Task<IActionResult> Index([FromBody] ApiStockRequest request)
        {
            ApiStockResponse response = null;
            try
            {
                response = await stockRepository.ActualizaStock(request);
                return Ok(response);
            }
            catch (Exception ex)
            {

                response = new ApiStockResponse();
                response.Estado = false;
                response.Mensaje = ex.Message;
                return NotFound(response);
            }

        }
        [HttpPost("get")]
        public async Task<IActionResult> GetAll()
        {
           
            try
            {
                var response = await stockRepository.MostarStock();
                return Ok(response);
            }
            catch (Exception ex)
            {

                
                return NotFound(ex);
            }

        }
    }
}