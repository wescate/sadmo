﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SADMO.Mantenimiento.Infraestructure.Data.Data;
using SADMO.Mantenimiento.Infraestructure.Data.Interfaces;

namespace SADMO.AppWeb
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();
            services.AddCors();
            services.AddSingleton<IDaUsuario, DA_Usuario>()
                    .AddSingleton<IAlmacen, DA_Almacen>()
                    .AddSingleton<IStock, DA_Stock>()
                    .AddSingleton<IProducto, DA_Producto>()
                    .AddSingleton<IRfid, DA_Rfid>()
                    .AddSingleton<IReporte, DA_Reporte>()
                    .AddSingleton<ITranporte, DA_Transporte>()
                    .AddSingleton<ITablasGenerales, DA_TablasGenerales>();
          //  services.AddDefaultIdentity<IdentityUser>(options => options.SignIn.RequireConfirmedAccount = true)


        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseStaticFiles();
            List<string> urls =new List<string>();
            urls.Add("http://localhost:7416");
            urls.Add("http://sadmonetcore.azurewebsites.net/");


            app.UseCors(builder => builder
              .AllowAnyHeader()
              .AllowAnyMethod()
              .SetIsOriginAllowed((host) => true)
              .AllowCredentials()
          );
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Security}/{action=Login}/{id?}");
            });
        }
    }
}
