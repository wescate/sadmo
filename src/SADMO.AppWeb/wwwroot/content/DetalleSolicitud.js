﻿var productoSeleccionado;
$(document).ready(function () {
    $(".btnEliminar").on('click', function (e) {

        Swal.fire({
            title: '¿ Desea eliminar producto ?',
            text: "Elimina productos de la Solicitud de Entrega",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Sí',
            cancelButtonText: 'No'
        }).then((result) => {
            if (result.value) {
                productoSeleccionado = this.getAttribute("id-producto");
                var resquest = {
                    NroSolicitud: $("#NroDoc").val(),
                    IdProducto: productoSeleccionado
                }

                var urlAjax = "/Solicitud/EliminarProductoSolicitud"

                CallAjaxMethod("Delete", urlAjax, resquest, ResultadoEliminar)
                $(this).closest('tr').remove();
            }
        })
    });

    function ResultadoEliminar(rpta) {
        if (rpta == 1) {

            $("#" + index).remove();
            Swal.fire(
                'Respuesta',
                'Registro eliminado de manera exitosa',
                'success'
            )
            document.location = '/Solicitud/DetalleSolicitud' + '?' + 'nroSolicitud=' + $("#NroDoc").val();
        }
    }

    $("#btnAgregar").on('click', function (e) {

        var listaMarcados = [];
        var cantidad = 0;
        $("input[type=checkbox]:checked").each(function () {
            cantidad = $("#cantidad_" + this.getAttribute("codigo-prod")).val()
            var ProductosNuevos = {
                idProducto: this.getAttribute("codigo-prod"),
                Cantidad: cantidad,
                NumeroSolicitud: $("#NroDoc").val()
            }
            listaMarcados.push(ProductosNuevos);
        });

        if (listaMarcados.length>0 ) {

            var valida = 0;

            for (var i = 0; i < listaMarcados.length; i++) {
                if (listaMarcados[i].Cantidad == "" || listaMarcados[i].Cantidad <= 0) {
                    valida += 1;
                }
            }

            if (valida> 0) {
                Swal.fire(
                    'Mensaje',
                    'Ingrese la cantidad para los productos seleccionados',
                    'error'
                )
            } else {
                var urlAjax = "/Solicitud/AgregarProductoSolicitud"
                var data = {
                    productoSolicitud: listaMarcados
                }
                CallAjaxMethod("Post", urlAjax, data, ResultadoGrabarProductos)
            } 
        } else {
            Swal.fire(
                'Mensaje',
                'No ha seleccionado productos nuevos',
                'error'
            )
        }
    });
    function ResultadoGrabarProductos(registrosGrabados) {
        if (registrosGrabados > 0) {
            Swal.fire(
                'Mensaje',
                'Se agregaron ' + registrosGrabados + ' productos a la solicitud ',
                'success'
            )

            $('#productoModal').modal('hide')
            document.location = '/Solicitud/DetalleSolicitud' + '?' + 'nroSolicitud=' + $("#NroDoc").val();
                 
        }
    }
    $("#btnCancelar").on('click', function (e) {
        Swal.fire({
            title: 'Cancelar',
            text: "Desea cancelar la Modificación de la solicitud " + $("#NroDoc").val(),
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Sí'
        }).then((result) => {
            if (result.value) {
                RegresarBusqueda();
            }
        })
    });
     
    
    $("#btnGrabarSolicitud").on('click', function (e) {
        var numeroSolicitud = $("#NroDoc").val();
        var resquest = {
            Estado: parseInt($("#Estado option:selected").val()), 
            FormaEntrega: $("#FormaEntrega option:selected").val(), 
            CodigoUbigeo: $("#Departamento option:selected").val() +
                $("#Provincia option:selected").val() +
                $("#Distrito option:selected").val(),
            NumeroSolicitud: numeroSolicitud,
            DireccionEntrega: $("#DireccionDestino").val(),
            AgenciaTransporte: parseInt($("#AgenciaTransporte option:selected").val())

        }
        var mensaje = "";
        // Si la SOlcitud se rechaza o se anula
        if (parseInt($("#Estado option:selected").val()) == 3 || parseInt($("#Estado option:selected").val()) == 6 ) {
            mensaje = "Solo se actualizara el estado de la Solictud " + numeroSolicitud;
        } else { 
            mensaje = "Desea grabar los datos de solcitud " + numeroSolicitud;
        }
     
        Swal.fire({
            title: 'Actualizar Solicitud',
            text: mensaje,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Sí'
        }).then((result) => {
            if (result.value) {
                var urlAjax = "/Solicitud/ModificarDatosSolicitud";
                CallAjaxMethod("Post", urlAjax, resquest, ResultadoGrabarSolicitud)
            }
        })
    });

    function ResultadoGrabarSolicitud(registroModificado) {
        if (registroModificado > 0) {
            Swal.fire({
                title: 'Mensaje',
                html: 'Se <b>agregaron o actualizaron</b> los datos de la solicitud ',
                icon: 'succes',
                showCancelButton: false,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Aceptar'
            }).then((result) => {
                if (result.value) {
                    RegresarBusqueda();
                }
            }) 
        }
    }
    function RegresarBusqueda() {
        document.location = '/Solicitud/SolicitudEntrega'
    }

    $("#Estado").on('change', function (e) {
        var fechaEmision = $("#FechaEmision").val();
        
        var hoy = new Date();
        var dd = hoy.getDate();
        var mm = hoy.getMonth() + 1;
        var yyyy = hoy.getFullYear();
        var fechaHoy = addZero(dd) + '/' + addZero(mm) + '/' + addZero(yyyy) 
        if (this.value == 3 ) {
            if (fechaEmision != fechaHoy ) {
                $("#btnGrabarSolicitud").removeClass("invisible");
            } else {
               // $("#btnGrabarSolicitud").addClass("invisible");
            }
           
        } else {
            $("#btnGrabarSolicitud").removeClass("invisible");
        }
    });

    $("#AgenciaTransporte").on('change', function (e) {
        var selected = $("#AgenciaTransporte option:selected").val();
        if (selected == 0) {
            $("#FormaEntrega").removeAttr("disabled");
            $("#DireccionDestino").val($("#DireccionDestinoCache").val());
        } else {
            $("#FormaEntrega").attr("disabled", "disabled");

            
            var direccion = $('option:selected', this).attr('direccion');
            var ubigeo = $('option:selected', this).attr('ubigeo');
            $("#DireccionDestino").val(direccion);
            $("#Departamento").val(ubigeo.substring(0, 2));
            BuscarProvincia();
            $("#Provincia").val(ubigeo.substring(2, 4));

            $("#Distrito").val(ubigeo.substring(4, 6));
        }


    })


    $("#FormaEntrega").on('change', function (e) {
        var selected = $("#FormaEntrega option:selected").val();
        if (selected == 3) {
            $("#AgenciaTransporte").removeAttr("disabled");
        } else {
            $("#AgenciaTransporte").attr("disabled", "disabled");
        }
    });

});