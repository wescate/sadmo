﻿//Funcion para formatear fecha
$.fn.datepicker.defaults.format = "dd/mm/yyyy";


var _CodigoProvincia;

function CallAjaxMethod(method, urlAjax, dataAjax, responseAjax) {

    $.ajax({
        type: method,
        dataType: 'json',
        cache: false,
        url: urlAjax,
        data: dataAjax,
        async:true,
        success: function (response) {
            responseAjax(response);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert('Error - ' + errorThrown);
        }
    });
}

function CallAjaxMethodNotAsync(method, urlAjax, dataAjax, responseAjax) {

    $.ajax({
        type: method,
        dataType: 'json',
        cache: false,
        url: urlAjax,
        data: dataAjax,
        async: false,
        success: function (response) {
            responseAjax(response);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert('Error - ' + errorThrown);
        }
    });
}
 
function addZero(i) {
    if (i < 10) {
        i = '0' + i;
    }
    return i;
}

function ClearRowTable() {
    $('#tbBody tr').remove();
}


$("#Departamento").on('change', function (e) {
    BuscarProvincia();
});

function BuscarProvincia() {
    var selected = $("#Departamento option:selected").val();
    if (selected != 0) {
        var urlAjax = "/Solicitud/BuscarDetalleUbigeo"
        var resquest = {
            Modo: 1,
            Codigo: selected
        }
        CallAjaxMethodNotAsync("get", urlAjax, resquest, MostrarProvincias)
    }
}

$("#Provincia").on('change', function (e) {
    BuscarDistrito();

});
function BuscarDistrito(CodigoProvincia) {
    var dptoSelected = $("#Departamento option:selected").val();
    var selected = $("#Provincia option:selected").val();
    if (selected != 0) {
        var urlAjax = "/Solicitud/BuscarDetalleUbigeo"
        var resquest = {
            Modo: 2,
            Codigo: dptoSelected + selected
        }
        CallAjaxMethodNotAsync("get", urlAjax, resquest, MostrarDistrito)
    }
}
function MostrarProvincias(response) {
    $('#Provincia option').remove();
    var rows = "";
    $.each(response, function (i, data) {
        rows += "<option value='" + data.codProvinciaUbigeo + "'>" + data.nombreUbigeo + "</option>";
    });

    $("#Provincia").append(rows);
    BuscarDistrito();
}
function MostrarDistrito(response) {
    $('#Distrito option').remove();
    var rows = "";
    $.each(response, function (i, data) {
        rows += "<option value='" + data.codDistritoUbigeo + "'>" + data.nombreUbigeo + "</option>";
    });

    $("#Distrito").append(rows);
}