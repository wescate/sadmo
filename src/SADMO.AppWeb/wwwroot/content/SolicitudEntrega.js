﻿$(document).ready(function () {
    $("#btnReset").on('click', function (e) {
        ClearRowTable();
    });

    $("#btnBuscar").on('click', function (e) {
        ClearRowTable();
        var _fechaInicio = "";
        var _fechaFin = "";

        if ($("#fechaInicio").val() != "" && $("#fechaFin").val() != "") {
            var fechaInicioSplited = $("#fechaInicio").val().split('/');
            _fechaInicio = fechaInicioSplited[2] + fechaInicioSplited[1] + fechaInicioSplited[0]
            var fechaFinSplited = $("#fechaFin").val().split('/');
            _fechaFin = fechaFinSplited[2] + fechaFinSplited[1] + fechaFinSplited[0]
        }
        var resquest = {
            CodigoSolTiempoEntrega: $("#nroSolicitud").val(),
            CodigoEstado: parseInt($("#estadoDocumento option:selected").val()),
            FechaEmisionInicio: _fechaInicio,
            FechaEmisionFin: _fechaFin,
            RazonSocial: $("#cliente").val(),
            FormaEntrega: $("#formaEntrega option:selected").val(),
            TipoDocumentoVenta: $("#tipoDocumento option:selected").val(),
            NumeroDocumentoVenta: $("#numeroDocumentoVenta").val(),
            CodigoUbigeo: $("#Departamento option:selected").val() +  
                          $("#Provincia option:selected").val() +
                          $("#Distrito option:selected").val()

        }

        if (validaModelo(resquest)) {

            var urlAjax = "/Solicitud/BuscarSolicitudEntrega"

            CallAjaxMethod("get", urlAjax, resquest, MostarDatos)
        } else {
            Swal.fire(
                '',
                'Ingresar un criterio de búsqueda',
                'error'
            )
        }

    });
    function validaModelo(model) {
        if (model.CodigoSolTiempoEntrega.trim() === ""
            && model.CodigoEstado === 0
            && model.FechaEmisionInicio.trim() === ""
            && model.FechaEmisionFin.trim() === ""            
            && model.RazonSocial.trim() === ""
            && model.FormaEntrega === "0"
            && model.TipoDocumentoVenta === "0"
            && model.NumeroDocumentoVenta.trim() === ""
            && model.CodigoUbigeo.trim() === "000"
           ) {
            return false;
        }
        return true;
    }
    function MostarDatos(response) {
        ClearRowTable();
        var rows = "";
        if (response.length > 0) {
            $.each(response, function (i, data) {
                rows += "<tr>";
                rows += "<td>" + data.codigoSolTiempoEntrega + "</td>";
                rows += "<td>" + data.descripcionEstado + "</td>";
                rows += "<td>" + data.descripcionFormaEntrega + "</td>";
                rows += "<td>" + data.razonSocial + "</td>";
                rows += "<td>   <a role='button' href='/Solicitud/DetalleSolicitud?nroSolicitud=" + data.codigoSolTiempoEntrega + "'   class='btn btn-outline-info btn-sm'><span class='glyphicon glyphicon-edit'> <span/> </a> </td>";
                rows += "</tr>";

            });

        } else {
            rows += "<tr>";
            rows += "<td colspan='5'> No hay registros </td>";
            rows += "</tr>";
        }
        $("#tbBody").append(rows);
    }

 
});