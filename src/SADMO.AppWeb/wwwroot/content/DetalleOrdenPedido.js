﻿$("#btnGrabarSolicitud").on('click', function (e) {
    var numeroSolicitud = $("#OrdenPedido").val();
    var resquest = {
        CodigoEstado: parseInt($("#Estado option:selected").val()),       
        CodigoOrdenPedido: parseInt(numeroSolicitud)

    }
    var mensaje = "";
    // Si la SOlcitud se rechaza o se anula
    if (parseInt($("#Estado option:selected").val()) == 3 || parseInt($("#Estado option:selected").val()) == 6) {
        mensaje = "Solo se actualizara el estado de la Solictud " + numeroSolicitud;
    } else {
        mensaje = "Desea grabar los datos de solcitud " + numeroSolicitud;
    }

    Swal.fire({
        title: 'Actualizar Orden Pedido',
        text: mensaje,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Sí'
    }).then((result) => {
        if (result.value) {
            var urlAjax = "/Solicitud/ModificarOrdenPedido";
            CallAjaxMethod("Post", urlAjax, resquest, ResultadoGrabarSolicitud)
        }
    })
});

function ResultadoGrabarSolicitud(registroModificado) {
    if (registroModificado > 0) {
        Swal.fire({
            title: 'Mensaje',
            html: 'Se <b>actualizaron</b> de la Orden de Pedido ',
            icon: 'succes',
            showCancelButton: false,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Aceptar'
        }).then((result) => {
            if (result.value) {
                RegresarBusqueda();
            }
        })
    }
}
function RegresarBusqueda() {
    document.location = '/Solicitud/OrdenPedido'
}
