﻿window.onload = function () {
    if (navigator.geolocation) {


        initMap();

    } else {
        alert("Sorry, this browser doesn't support geolocation!");
    }
}
function initMap() {
    var directionsRenderer = new google.maps.DirectionsRenderer();
    var directionsRenderer2 = new google.maps.DirectionsRenderer();
    var directionsService = new google.maps.DirectionsService();
    var directionsService2 = new google.maps.DirectionsService();

    var map = new google.maps.Map(document.getElementById("map"), {
        zoom: 14,
        center: { lat: -12.0428158, lng: -77.0772169 }
    });

    var map2 = new google.maps.Map(document.getElementById("map2"), {
        zoom: 14,
        center: { lat: -12.0428158, lng: -77.0772169 }
    });

    directionsRenderer.setMap(map);
    directionsRenderer2.setMap(map2)

    calculateAndDisplayRoute(directionsService, directionsRenderer);
    calculateAndDisplayRealRoute(directionsService2, directionsRenderer2);


}
function calculateAndDisplayRealRoute(directionsService, directionsRenderer) {
    var wayp = document.getElementById("rutaReal").value;
    var waypts = [];
    var arreglo = wayp.split("|");
    var destino = null;
    for (var i = 0; i < arreglo.length; i++) {
        var coor = arreglo[i].split(",");
        if (i == arreglo.length -1 ) {
            destino = { lat: parseFloat(coor[0]), lng: parseFloat(coor[1]) };
        }
        waypts.push({
            location: { lat: parseFloat(coor[0]), lng: parseFloat(coor[1]) },
            stopover: true
        });
    }

    directionsService.route(
        {
            origin: { lat: -12.0428158, lng: -77.0772169 }, // Haight.
            destination: destino,
            waypoints: waypts,
            optimizeWaypoints: true,
            // Note that Javascript allows us to access the constant
            // using square brackets and a string value as its
            // "property."
            travelMode: 'DRIVING'
        },
        function (response, status) {
            if (status == "OK") {
             
                directionsRenderer.setDirections(response);
            } else {
                window.alert("Directions request failed due to " + status);
            }
        }
    );
}
function calculateAndDisplayRoute(directionsService, directionsRenderer) {
    
    directionsService.route(
        {
            origin: { lat: -12.0428158, lng: -77.0772169 }, // Haight.
            destination: { lat: parseFloat(document.getElementById("latitud").value), lng: parseFloat(document.getElementById("longitud").value) }, // Ocean Beach.
            // Note that Javascript allows us to access the constant
            // using square brackets and a string value as its
            // "property."
            travelMode: 'DRIVING'
        },
        function (response, status) {
            if (status == "OK") {
                directionsRenderer.setDirections(response);
            } else {
                window.alert("Directions request failed due to " + status);
            }
        }
    );
}