﻿var productoSeleccionado;
var direccionCache = "";
$(document).ready(function () {
    $(".btnEliminar").on('click', function (e) {
      
        Swal.fire({
            title: '¿ Desea eliminar producto ?',
            text: "Elimina productos de la venta",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Sí',
            cancelButtonText: 'No'
        }).then((result) => {
            if (result.value) { 
                productoSeleccionado = this.getAttribute("id-producto");
                var resquest = {
                    NroDocumentoVenta: $("#docVenta").val(),
                    IdProducto: productoSeleccionado
                }

                var urlAjax = "/Solicitud/EliminarProductoVenta"

                CallAjaxMethod("Delete", urlAjax, resquest, ResultadoEliminar)
                $(this).closest('tr').remove();
            }
        })
    });

    function ResultadoEliminar(rpta) {
        if (rpta == 1) {
            
            $("#" + index).remove();
            Swal.fire(
                'Respuesta',
                'Registro eliminado de manera exitosa',
                'Éxito'
            )
        } 
    }

    $("#btnAgregar").on('click', function (e) {

        var listaMarcados = [];
        var cantidad = 0;
        $("input[type=checkbox]:checked").each(function () {
            cantidad = $("#cantidad_" + this.getAttribute("codigo-prod")).val()
            var ProductosNuevos = {
                idProducto: this.getAttribute("codigo-prod"),
                Cantidad: cantidad,
                NumeroSolicitud: $("#docVenta").val()
            }
            listaMarcados.push(ProductosNuevos);
        });

        if (listaMarcados.length > 0) {

            var valida = 0;

            for (var i = 0; i < listaMarcados.length; i++) {
                if (listaMarcados[i].Cantidad == "" || listaMarcados[i].Cantidad <= 0) {
                    valida += 1;
                }
            }

            if (valida > 0) {
                Swal.fire(
                    'Mensaje',
                    'Ingrese la cantidad para los productos seleccionados',
                    'error'
                )
            } else {
                var urlAjax = "/Solicitud/AgregarProductoVenta"
                var data = {
                    productoSolicitud: listaMarcados
                }
                CallAjaxMethod("Post", urlAjax, data, ResultadoGrabarProductos)
            }
        } else {
            Swal.fire(
                'Mensaje',
                'No ha seleccionado productos nuevos',
                'error'
            )
        }
    });
    function ResultadoGrabarProductos(registrosGrabados) {
        if (registrosGrabados > 0) {
            Swal.fire(
                'Mensaje',
                'Se agregaron ' + registrosGrabados + ' productos a la solicitud ',
                'success'
            )

            $('#productoModal').modal('hide')
            document.location = '/Solicitud/DetalleVenta/' + $("#docVenta").val();

        }
    }
    $("#btnCancelar").on('click', function (e) {
        Swal.fire({
            title: 'Cancelar',
            text: "Desea cancelar la generación de solicitud " + $("#NroDoc").val(),
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Sí'
        }).then((result) => {
            if (result.value) {
                RegresarBusqueda();
            }
        })
    });
    function RegresarBusqueda() {
        document.location = '/Solicitud/BuscarVenta'
    }

    $("#btnGrabarSolicitud").on('click', function (e) {
        Swal.fire({
            title: 'Crear Solicitud de Entrega',
            text: "¿ Desea generar la solicitud de entrega ?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Sí'
        }).then((result) => {
            if (result.value) {
                GenerarSolicitud();
            }
        })
    });
    function GenerarSolicitud() {
        var urlAjax = "/Solicitud/GenerarSolicitud"
        var data = {
           // CodigoSolTiempoEntrega: 0,  
            CodigoTipoVenta: $("#TipoDocVenta option:selected").val(),
            NumeroDocVenta: $("#docVenta").val(),
            //CodigoCliente: 0,
           // FechaEmisionSolTiempoEntrega: null,
            DirDestinoSolTiempoEntrega: $("#DireccionDestino").val(),
            CodigoUbigeo: $("#Departamento option:selected").val() +
                          $("#Provincia option:selected").val() +
                          $("#Distrito option:selected").val(),
            CodigoFormaEntrega: $("#FormaEntrega option:selected").val(),
            CodigoAgenciaTransporte: $("#AgenciaTransporte option:selected").val(),
            tbEstado_CodigoEstado: parseInt($("#Estado option:selected").val()),
            ObservacionSolTiempoEntrega: $("#Observacion").val()
        }
        CallAjaxMethod("Post", urlAjax, data, ResultadoGrabarSolicitud)
    }

    function ResultadoGrabarSolicitud(result) {
        $("#TiempoEntrega").val(result.fechaEntregaEstimada);
        Swal.fire({
            title: 'Mensaje',
            html: 'Se generó la solicitud ' + result.codigoSolicitud + '. El tiempo de entrega estimado es : <b>' + result.fechaEntregaEstimada + '</b>',
            icon: 'succes',
            showCancelButton: false,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Aceptar'
        }).then((result) => {
            if (result.value) {
                document.location = '/Solicitud/SolicitudEntrega'
            }
        })
        
    }

    $("#AgenciaTransporte").on('change', function (e) {
        var selected = $("#AgenciaTransporte option:selected").val();
        if (selected == 0) {
            $("#FormaEntrega").removeAttr("disabled");
            $("#DireccionDestino").val($("#DireccionDestinoCache").val());
        } else {
            $("#FormaEntrega").attr("disabled", "disabled");
          
            
            var direccion = $('option:selected', this).attr('direccion');
            var ubigeo = $('option:selected', this).attr('ubigeo');
            $("#DireccionDestino").val(direccion);
            $("#Departamento").val(ubigeo.substring(0, 2));
            BuscarProvincia();
            $("#Provincia").val(ubigeo.substring(2, 4));

            $("#Distrito").val(ubigeo.substring(4, 6));
        }

      
    })


    $("#FormaEntrega").on('change', function (e) {
        var selected = $("#FormaEntrega option:selected").val();
        if (selected == 3) {
            $("#AgenciaTransporte").removeAttr("disabled");
        } else {
            $("#AgenciaTransporte").attr("disabled", "disabled");
        }
    });
});