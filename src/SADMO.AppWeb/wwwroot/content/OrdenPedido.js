﻿$(document).ready(function () {
    $("#btnReset").on('click', function (e) {
        ClearRowTable();
    });

    $("#btnBuscar").on('click', function (e) {
        ClearRowTable();
        var _fechaInicio = "";
        var _fechaFin = "";

        if ($("#fechaInicio").val() != "" && $("#fechaFin").val() != "") {
            var fechaInicioSplited = $("#fechaInicio").val().split('/');
            _fechaInicio = fechaInicioSplited[2] + fechaInicioSplited[1] + fechaInicioSplited[0];
            var fechaFinSplited = $("#fechaFin").val().split('/');
            _fechaFin = fechaFinSplited[2] + fechaFinSplited[1] + fechaFinSplited[0]; 
        }


        var resquest = {
            OrdenPedido: $("#ordenPedido").val(),
            CodigoSolTiempoEntrega: $("#nroSolicitud").val(),
            FechaEmisionInicio: _fechaInicio,
            FechaEmisionFin: _fechaFin,
            TipoDocumentoVenta: $("#tipoDocumento option:selected").val(),
            NumeroDocumentoVenta: $("#numeroDocumentoVenta").val(),
            CodigoEstado: parseInt($("#estadoDocumento option:selected").val()),
            FormaEntrega: $("#formaEntrega option:selected").val(),
            RazonSocial: $("#cliente").val()
        }

        if (validaModelo(resquest)) {
            var urlAjax = "/Solicitud/BuscarOrdenPedido";
            CallAjaxMethod("get", urlAjax, resquest, MostarDatos);
        } else {
            Swal.fire(
                '',
                'Ingresar un criterio de búsqueda',
                'error'
            )
        }
    

    });
    function validaModelo(model) {
        if (model.OrdenPedido.trim() === ""    
            && model.CodigoSolTiempoEntrega.trim() === ""
            && model.FechaEmisionInicio.trim() === ""
            && model.FechaEmisionFin.trim() === ""
            && model.TipoDocumentoVenta === "0"
            && model.NumeroDocumentoVenta.trim() === "" 
            && model.CodigoEstado === 0
            && model.FormaEntrega === "0"
            && model.RazonSocial.trim() === ""
        ) {
            return false;
        }
        return true;
    }
    function MostarDatos(response) {
        ClearRowTable();
        var rows = "";
        if (response.length > 0) {
            $.each(response, function (i, data) {
                rows += "<tr>";
                rows += "<td>" + data.numeroDocumento + "</td>";
                rows += "<td>" + data.estado + "</td>"; 
                rows += "<td>" + data.razonSocial + "</td>";
                rows += "<td> ";
                rows += "<a role='button' href='/Solicitud/DetalleOrdenPedido?OrdenPedido=" + data.numeroDocumento + "&Modo=E'   class='btn btn-outline-info btn-sm'><span class='glyphicon glyphicon-edit'> <span/> </a>&nbsp;";
                rows += "<a role='button' href='/Solicitud/DetalleOrdenPedido?OrdenPedido=" + data.numeroDocumento + "&Modo=L'   class='btn btn-outline-info btn-sm'><span class='glyphicon glyphicon-eye-open'> <span/> </a>";
                rows += "</td>";
                rows += "</tr>";

            });

        } else {
            rows += "<tr>";
            rows += "<td colspan='5'> No hay registros </td>";
            rows += "</tr>";
        }
        $("#tbBody").append(rows);
    }
});