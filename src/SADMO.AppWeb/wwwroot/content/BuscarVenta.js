﻿ 
$(document).ready(function () {
    
    $("#btnReset").on('click', function (e) {
       ClearRowTable();
    });
    $("#btnBuscar").on('click', function (e) {
        ClearRowTable();
        var _fechaInicio = "";
        var _fechaFin = "";
        if ($("#fechaInicio").val() != "" && $("#fechaFin").val() != "") {
            var fechaInicioSplited = $("#fechaInicio").val().split('/');
            _fechaInicio = fechaInicioSplited[2] + fechaInicioSplited[1] + fechaInicioSplited[0]
            var fechaFinSplited = $("#fechaFin").val().split('/');
            _fechaFin = fechaFinSplited[2] + fechaFinSplited[1] + fechaFinSplited[0]
        }
     

        var resquest = {
            Cliente :  $("#cliente").val(),
            TipoDocumento: parseInt($("#tipoDocumento option:selected").val()),
            NumeroDocumento: $("#numeroDocumento").val(),
            TipoDocumentoVenta: $("#tipoDocumentoVenta option:selected").val(),
            NumeroDocumentoVenta: $("#numeroDocumentoVenta").val(),
            FechaEmisionInicio: _fechaInicio,
            FechaEmisionFin: _fechaFin,

        }


        if (validaModelo(resquest)) {
            var urlAjax = "/Solicitud/BuscarVentaPorCriterio"

            CallAjaxMethod("get", urlAjax, resquest, MostarDatos)
        } else {
            Swal.fire(
                '',
                'Ingresar un criterio de búsqueda',
                'error'
            )
        }
     
    });

    function validaModelo(model) {
        if (model.Cliente.trim() === ""
            && model.TipoDocumento === 0
            && model.NumeroDocumento.trim() === ""
            && model.TipoDocumentoVenta === "0"
            && model.NumeroDocumentoVenta.trim() === ""
            && model.FechaEmisionInicio.trim() === ""
            && model.FechaEmisionFin.trim() ==="") {
            return false;
        }
        return true;
    }

    function MostarDatos(response) { 
        ClearRowTable();
        var rows = "";
        if (response.length >  0) { 
            $.each(response, function (i, data) {
                rows += "<tr>";
                rows += "<td>" + data.fechaEmision + "</td>";
                rows += "<td>" + data.descripcionTipoVenta + "</td>";
                rows += "<td>" + data.numeroDocVenta + "</td>";
                rows += "<td>" + data.razonSocial + "</td>";
                rows += "<td>   <a role='button' href='/Solicitud/DetalleVenta/" + data.numeroDocVenta + "'   class='btn btn-outline-info btn-sm'>Ver</a> </td>";
                rows += "</tr>";
           
            });
       
        } else {
            rows += "<tr>";
            rows += "<td colspan='5'> No hay registros </td>";
            rows += "</tr>";
        }
        $("#tbBody").append(rows);
    } 


    
});

 
