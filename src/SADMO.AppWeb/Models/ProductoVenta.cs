﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SADMO.AppWeb.Models
{
    public class ProductoVenta
    {
        public string NroDocumentoVenta { get; set; }
        public string IdProducto { get; set; }
    }

    public class ProductoSolicitud
    {
        public string NroSolicitud { get; set; }
        public string IdProducto { get; set; }
    }
}
