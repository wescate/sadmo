﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SADMO.AppWeb.Models
{
    public class BucarSolicitudTiempoEntrega
    {
        public string CodigoSolTiempoEntrega { get; set; }
        public int CodigoEstado { get; set; }
        public string FechaEmisionInicio { get; set; }
        public string FechaEmisionFin { get; set; }
        public string RazonSocial { get; set; }
        public int FormaEntrega { get; set; }
        public string TipoDocumentoVenta { get; set; }
        public string NumeroDocumentoVenta { get; set; }
        public string CodigoUbigeo { get; set; }
    }
}
