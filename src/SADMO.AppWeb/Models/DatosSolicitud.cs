﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SADMO.AppWeb.Models
{
    public class DatosSolicitud
    {
        public int Estado { get; set; }
        public int FormaEntrega { get; set; }
        public string CodigoUbigeo { get; set; }
        public string  NumeroSolicitud { get; set; }
        public string DireccionEntrega { get; set; }
        public int AgenciaTransporte { get; set; }
    }
}
