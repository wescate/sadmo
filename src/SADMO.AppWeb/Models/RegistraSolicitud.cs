﻿using SADMO.Mantenimiento.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SADMO.AppWeb.Models
{
    public class RegistraProductosDocumento
    {
        public IEnumerable<RegistrarProductoSolicitud> productoSolicitud { get; set; }
    }
}
