﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SADMO.AppWeb.Models
{
    public class UbigeoDistrito
    {
        public string Codigo { get; set; }
        public string Nombre { get; set; }

    }
}
