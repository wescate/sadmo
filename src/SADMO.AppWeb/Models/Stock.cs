﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SADMO.AppWeb.Models
{
    public class Stock
    {
        public string NombreAlmacen { get; set; }
        public string DescripcionProducto { get; set; } 
        public decimal? CantidadStock { get; set; }
    }
}
