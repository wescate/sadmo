﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SADMO.AppWeb.Models
{
    public class DetalleUbigeo
    {
        public int Modo { get; set; }
        public string Codigo { get; set; }
    }
}
