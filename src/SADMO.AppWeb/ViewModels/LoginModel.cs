﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SADMO.AppWeb.ViewModels
{
    public class LoginModel
    {
        public LoginModel()
        {
            Password = "";
            Usuario = "";
        }


        public string Usuario { get; set; }
        public string Password { get; set; }
    }
}
