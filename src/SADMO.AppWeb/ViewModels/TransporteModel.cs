﻿using SADMO.Mantenimiento.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SADMO.AppWeb.ViewModels
{
    public class TransporteModel
    {
        public vwTrasnportistaOrdenPedido RutaEstimada { get; set; }
        public IEnumerable<tbRutaReal> RutaReal { get; set; }
        public string Coordenadas { get; set; }
    }
}
