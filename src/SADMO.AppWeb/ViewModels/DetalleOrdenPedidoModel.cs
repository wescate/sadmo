﻿using SADMO.AppWeb.Models;
using SADMO.Mantenimiento.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SADMO.AppWeb.ViewModels
{
    public class DetalleOrdenPedidoModel
    {
        public IEnumerable<tbTipoDocumentoIdentidad> DocumentosIdentidad { get; set; }
        public IEnumerable<tbTipoDocVenta> DocumentosVenta { get; set; }
        public IEnumerable<tbEstado> Estados { get; set; }
        public IEnumerable<UbigeoDepartamento> Departamentos { get; set; }
        public IEnumerable<UbigeoProvincia> Provincias { get; set; }
        public IEnumerable<UbigeoDistrito> Distritos { get; set; }
        public IEnumerable<tbFormaEntrega> FormaEntregas { get; set; }
        public uspDetalleOrdenPedidoResponse DetalleOP { get; set; }
        public string ModoEnable { get; set; }
    }
}
