﻿using SADMO.Mantenimiento.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SADMO.AppWeb.ViewModels
{
    public class RfidModel
    {
        public IEnumerable<uspListarRFID> RFIDs { get; set; }
        public string TagRfid { get; set; }
        public string CodInterno { get; set; } 
    }
}
