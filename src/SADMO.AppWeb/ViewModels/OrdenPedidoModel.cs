﻿
using SADMO.Mantenimiento.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SADMO.AppWeb.ViewModels
{
    public class OrdenPedidoModel
    {
        public IEnumerable<tbEstado> Estados { get; set; } 
        public IEnumerable<tbFormaEntrega> FormasEntrega { get; set; }
        public IEnumerable<tbTipoDocVenta> DocumentosVenta { get; set; }

    }
}
