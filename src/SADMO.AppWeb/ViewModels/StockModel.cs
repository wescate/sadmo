﻿using SADMO.Mantenimiento.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SADMO.AppWeb.ViewModels
{
    public class StockModel
    {
        public IEnumerable<tbAlmacen> Almacenes { get; set; }
        public IEnumerable<tbProducto> Productos { get; set; }

        public IEnumerable<tbKardex> Movimientos { get; set; }
        public IEnumerable<Models.Stock> Stocks { get; set; }
    }
}
