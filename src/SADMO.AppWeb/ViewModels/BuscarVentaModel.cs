﻿using SADMO.Mantenimiento.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SADMO.AppWeb.ViewModels
{
    public class BuscarVentaModel
    {
        public IEnumerable<tbTipoDocumentoIdentidad> DocumentosIdentidad { get; set; }
        public IEnumerable<tbTipoDocVenta> DocumentosVenta { get; set; }
    }
}
