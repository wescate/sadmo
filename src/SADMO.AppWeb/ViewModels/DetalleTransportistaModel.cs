﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SADMO.AppWeb.ViewModels
{
    public class DetalleTransportistaModel
    {
        public decimal  LatIni  { get; set; }
        public decimal LatFin { get; set; }
        public decimal LongIni { get; set; }
        public decimal LongFin { get; set; }

    }
}
