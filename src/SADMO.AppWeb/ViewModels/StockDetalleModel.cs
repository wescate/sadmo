﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SADMO.AppWeb.ViewModels
{
    public class StockDetalleModel
    {
        public string NombreAlmacen { get; set; }
        public string DescripcionProducto { get; set; }

        public IEnumerable<Models.DetalleStock> DetalleStock { get; set; }
    }
}