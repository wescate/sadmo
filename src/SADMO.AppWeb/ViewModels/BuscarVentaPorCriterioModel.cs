﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SADMO.AppWeb.ViewModels
{
    public class BuscarVentaPorCriterioModel
    {
        public string Cliente { get; set; }
        public int TipoDocumento { get; set; }
        public string NumeroDocumento { get; set; }
        public string TipoDocumentoVenta { get; set; }
        public string NumeroDocumentoVenta { get; set; }
        public string FechaEmisionInicio { get; set; }
        public string FechaEmisionFin { get; set; }
    }
}
