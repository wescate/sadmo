﻿using Microsoft.AspNetCore.Mvc;
using SADMO.AppWeb.ViewModels;
using SADMO.Mantenimiento.Domain;
using SADMO.Mantenimiento.Infraestructure.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SADMO.AppWeb.Controllers
{
    public class SecurityController: Controller
    {
        private readonly IDaUsuario usuarioRepository;

        public SecurityController(IDaUsuario usuarioRepository)
        {
            this.usuarioRepository = usuarioRepository;
        }

        public ActionResult Login()
        {
            
            return View();
        }
        [HttpPost]
        public  async Task<ActionResult> Home(LoginModel model)
        {
           
                var usuario = await usuarioRepository.GetUsuario(model.Usuario, model.Password);

                if (usuario != null)
                {
                return View(usuario);
            }
                else
                {
                 return View("Login");
                }
            
           

           
        }

       
    }
}
