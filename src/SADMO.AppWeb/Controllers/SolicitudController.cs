﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SADMO.AppWeb.Models;
using SADMO.AppWeb.ViewModels;
using SADMO.Mantenimiento.Domain;
using SADMO.Mantenimiento.Infraestructure.Data.Interfaces;

namespace SADMO.AppWeb.Controllers
{
    public class SolicitudController : Controller
    {

        private readonly ITablasGenerales tablasGeneralesRespositorio;
        private readonly IReporte reporteRepositorio;

        public SolicitudController(ITablasGenerales tablasGeneralesRespositorio, IReporte reporteRepositorio)
        {
            this.tablasGeneralesRespositorio = tablasGeneralesRespositorio;
            this.reporteRepositorio = reporteRepositorio;
        }

        public async Task<IActionResult> BuscarVenta()
        {
            BuscarVentaModel model = new BuscarVentaModel();
            model.DocumentosIdentidad = await tablasGeneralesRespositorio.ListarTipoDocumentoIdentidad();
            model.DocumentosVenta = await tablasGeneralesRespositorio.ListarTipoDocumentoVenta();

            return View(model);
        }

        [HttpGet]
        public async Task<JsonResult> BuscarVentaPorCriterio(BuscarVentaPorCriterioModel request) {
            var req = new BuscarVentaPorCriterio();
            req.Cliente = request.Cliente ?? "";
            req.NumeroDocumento = request.NumeroDocumento ?? "";
            req.NumeroDocumentoVenta = request.NumeroDocumentoVenta ?? "";
            req.FechaEmisionInicio = request.FechaEmisionInicio ?? "";
            req.FechaEmisionFin = request.FechaEmisionFin ?? "";
            req.TipoDocumento = request.TipoDocumento;
            req.TipoDocumentoVenta = request.TipoDocumentoVenta;
            var model = await reporteRepositorio.ObtenerDocumentoVenta(req);
            return Json(model);
        }

        public async Task<IActionResult> DetalleVenta(int id)
        {
            DetalleVentaModel model = new DetalleVentaModel();
            model.DocumentosIdentidad = await tablasGeneralesRespositorio.ListarTipoDocumentoIdentidad();
            model.DocumentosVenta = await tablasGeneralesRespositorio.ListarTipoDocumentoVenta();
            model.FormasEntrega = await tablasGeneralesRespositorio.ListarFormaEntrega();
            model.AgenciasTransporte = await tablasGeneralesRespositorio.ListarAgenciaTransporte();
            model.DetalleVenta = reporteRepositorio.ObtenerDetalleVenta(new uspSolicitudTiempoEntregaListar() { Id = id.ToString() });
            var ubigeos = await tablasGeneralesRespositorio.ListarUbigeos();
            model.Departamentos = (from x in ubigeos
                                   where x.CodProvinciaUbigeo == "00"
                                   && x.CodDistritoUbigeo == "00"
                                   select new UbigeoDepartamento() { Codigo = x.CodDepartamentoUbigeo, Nombre = x.NombreUbigeo }).Distinct().ToList();
            model.Provincias = (from x in ubigeos
                                where x.CodDistritoUbigeo == "00"
                               && x.CodDepartamentoUbigeo == model.DetalleVenta.Venta.CodigoUbigeo.Substring(0, 2)
                                select new UbigeoProvincia() { Codigo = x.CodProvinciaUbigeo, Nombre = x.NombreUbigeo }).Distinct().ToList();
            model.Distritos = (from x in ubigeos
                               where x.CodDepartamentoUbigeo == model.DetalleVenta.Venta.CodigoUbigeo.Substring(0, 2)
                              && x.CodProvinciaUbigeo == model.DetalleVenta.Venta.CodigoUbigeo.Substring(2, 2)
                               select new UbigeoDistrito() { Codigo = x.CodDistritoUbigeo, Nombre = x.NombreUbigeo }).Distinct().ToList();



            model.Estados = await tablasGeneralesRespositorio.ListarEstados();

            var _estado = model.Estados.ToList().Find(x => x.CodigoEstado == model.DetalleVenta.Venta.tbEstado_CodigoEstado);
            model.Estado = _estado.DescripcionEstado;
            model.ProductoVenta = await reporteRepositorio.ListarProductoVenta(id.ToString());
            return View(model);
        }

        [HttpDelete]
        public async Task<JsonResult> EliminarProductoVenta(ProductoVenta request)
        {
            var req = new uspEliminarProductoVenta();
            req.CodigoProducto = Convert.ToInt32(request.IdProducto);
            req.NumeroDocVenta = request.NroDocumentoVenta;
            int rowsAffected = await reporteRepositorio.EliminarProductoVenta(req);
            return Json(rowsAffected);
        }

        [HttpDelete]
        public async Task<JsonResult> EliminarProductoSolicitud(ProductoSolicitud request)
        {
            var req = new uspEliminarProductoSolicitudTE();
            req.CodigoProducto = Convert.ToInt32(request.IdProducto);
            req.NumeroSolicitud = request.NroSolicitud;
            int rowsAffected = await reporteRepositorio.EliminarProductoSolicitudTiempoEntrega(req);
            return Json(rowsAffected);
        }

        public async Task<IActionResult> SolicitudEntrega()
        {
            var model = new SolicitudEntregaModel();
            var ubigeos = await tablasGeneralesRespositorio.ListarUbigeos();
            model.Departamentos = (from x in ubigeos
                                   where x.CodProvinciaUbigeo == "00"
                                   && x.CodDistritoUbigeo == "00"
                                   select new UbigeoDepartamento() { Codigo = x.CodDepartamentoUbigeo, Nombre = x.NombreUbigeo }).Distinct().ToList();
            model.Provincias = (from x in ubigeos
                                select new UbigeoProvincia() { Codigo = x.CodProvinciaUbigeo, Nombre = x.NombreUbigeo }).Distinct().ToList();
            model.Distritos = (from x in ubigeos
                               select new UbigeoDistrito() { Codigo = x.CodDistritoUbigeo, Nombre = x.NombreUbigeo }).Distinct().ToList();

            model.Estados = await tablasGeneralesRespositorio.ListarEstados();
            model.DocumentosVenta = await tablasGeneralesRespositorio.ListarTipoDocumentoVenta();
            model.FormasEntrega = await tablasGeneralesRespositorio.ListarFormaEntrega();

            return View(model);
        }

        [HttpGet]
        public async Task<JsonResult> BuscarSolicitudEntrega(BucarSolicitudTiempoEntrega request)
        {
            var req = new uspBucarSolicitudTiempoEntrega();
            req.CodigoSolTiempoEntrega = request.CodigoSolTiempoEntrega ?? "";
            req.CodigoEstado = request.CodigoEstado;
            req.FechaEmisionInicio = request.FechaEmisionInicio ?? "";
            req.FechaEmisionFin = request.FechaEmisionFin ?? "";
            req.RazonSocial = request.RazonSocial ?? "";
            req.FormaEntrega = request.FormaEntrega;
            req.TipoDocumentoVenta = request.TipoDocumentoVenta;
            req.NumeroDocumentoVenta = request.NumeroDocumentoVenta ?? "";
            req.CodigoUbigeo = request.CodigoUbigeo;
            var model = await reporteRepositorio.ListarSolicitudesTE(req);
            return Json(model);
        }
        [HttpGet]
        public async Task<JsonResult> BuscarDetalleUbigeo(DetalleUbigeo request)
        {
            var model = await tablasGeneralesRespositorio.ListarDetalleUbigeos(request.Modo, request.Codigo);

            return Json(model);
        }

        public async Task<IActionResult> DetalleSolicitud() {
            string nroSolicitud = "";
            if (!String.IsNullOrEmpty(HttpContext.Request.Query["nroSolicitud"]))
                nroSolicitud = HttpContext.Request.Query["nroSolicitud"];

            var model = new DetalleSolicitudModel();
            model.DocumentosIdentidad = await tablasGeneralesRespositorio.ListarTipoDocumentoIdentidad();
            model.DocumentosVenta = await tablasGeneralesRespositorio.ListarTipoDocumentoVenta();
            var estados = await tablasGeneralesRespositorio.ListarEstados();
            model.Estados = estados.Where(x => x.DescripcionEstado.ToLower() != "transito" && x.DescripcionEstado.ToLower() != "despachado").ToList();
            model.FormaEntregas = await tablasGeneralesRespositorio.ListarFormaEntrega();
            model.DetalleSE = reporteRepositorio.ObtenerDetalleSolicitudTE(nroSolicitud);

            var ubigeos = await tablasGeneralesRespositorio.ListarUbigeos();
            model.Departamentos = (from x in ubigeos
                                   where x.CodProvinciaUbigeo == "00"
                                  && x.CodDistritoUbigeo == "00"
                                   select new UbigeoDepartamento() { Codigo = x.CodDepartamentoUbigeo, Nombre = x.NombreUbigeo }).Distinct().ToList();
            model.Provincias = (from x in ubigeos
                                where x.CodDistritoUbigeo == "00"
                                && x.CodDepartamentoUbigeo == model.DetalleSE.Solictud.CodigoUbigeo.Substring(0, 2)
                                select new UbigeoProvincia() { Codigo = x.CodProvinciaUbigeo, Nombre = x.NombreUbigeo }).Distinct().ToList();
            model.Distritos = (from x in ubigeos
                               where x.CodDepartamentoUbigeo == model.DetalleSE.Solictud.CodigoUbigeo.Substring(0, 2)
                               && x.CodProvinciaUbigeo == model.DetalleSE.Solictud.CodigoUbigeo.Substring(2, 2)
                               select new UbigeoDistrito() { Codigo = x.CodDistritoUbigeo, Nombre = x.NombreUbigeo }).Distinct().ToList();

            if (model.DetalleSE.Solictud.CodigoEstado == 2 || model.DetalleSE.Solictud.CodigoEstado == 5)
            {
                model.MostrarBotonGrabar = false;
            }
            else if (model.DetalleSE.Solictud.CodigoEstado == 3)
            {

                model.MostrarBotonGrabar = model.DetalleSE.Solictud.FechaEmisionSolTiempoEntrega == DateTime.Now.ToString("dd/MM/yyyy") ? true : false;
            }
            else
            {
                model.MostrarBotonGrabar = true;
            }
            model.AgenciasTransporte = await tablasGeneralesRespositorio.ListarAgenciaTransporte();
            return View(model);
        }
        [HttpPost]
        public async Task<JsonResult> AgregarProductoSolicitud(RegistraProductosDocumento registraSolicitud) {
            int registrosGrabados = await reporteRepositorio.RegistrarProductoSolicitud(registraSolicitud.productoSolicitud);
            return Json(registrosGrabados);
        }
        [HttpPost]
        public async Task<JsonResult> AgregarProductoVenta(RegistraProductosDocumento registraSolicitud)
        {
            int registrosGrabados = await reporteRepositorio.RegistrarProductoVenta(registraSolicitud.productoSolicitud);
            return Json(registrosGrabados);
        }
        [HttpPost]
        public async Task<JsonResult> ModificarDatosSolicitud(DatosSolicitud registraSolicitud)
        {
            uspModifcarSolicitud uspModifcar = new uspModifcarSolicitud();
            uspModifcar.DireccionEntrega = registraSolicitud.DireccionEntrega;
            uspModifcar.AgenciaTransporte = registraSolicitud.AgenciaTransporte;
            uspModifcar.CodigoUbigeo = registraSolicitud.CodigoUbigeo;
            uspModifcar.FormaEntrega = registraSolicitud.FormaEntrega;
            uspModifcar.Estado = registraSolicitud.Estado;
            uspModifcar.NumeroSolicitud = Convert.ToInt32(registraSolicitud.NumeroSolicitud);

            int registrosGrabados = await reporteRepositorio.ModificarDatosSolicitud(uspModifcar);
            return Json(registrosGrabados);
        }
        
        [HttpPost]
        public async Task<JsonResult> ModificarOrdenPedido(uspModifcarOrdenPedido registraSolicitud)
        { 
            int registrosGrabados = await reporteRepositorio.ModificarOrdenPedido(registraSolicitud);
            return Json(registrosGrabados);
        }
        public async  Task<ActionResult> OrdenPedido() {
            OrdenPedidoModel model = new OrdenPedidoModel();
            model.DocumentosVenta = await tablasGeneralesRespositorio.ListarTipoDocumentoVenta();
            model.Estados = await tablasGeneralesRespositorio.ListarEstados();
            model.FormasEntrega = await tablasGeneralesRespositorio.ListarFormaEntrega();
            return View(model);
        }

        //
        [HttpGet]
        public async Task<JsonResult> BuscarOrdenPedido(BucarOrdenPedido request)
        {
            var req = new uspListarOrdenPedido();
            req.CodigoOrdenPedido = request.OrdenPedido ?? "";
            req.CodigoSolTiempoEntrega = request.CodigoSolTiempoEntrega ?? "";
            req.CodigoEstado = request.CodigoEstado;
            req.FechaEmisionInicio = request.FechaEmisionInicio ?? "";
            req.FechaEmisionFin = request.FechaEmisionFin ?? "";
            req.RazonSocial = request.RazonSocial ?? "";
            req.FormaEntrega = request.FormaEntrega;
            req.TipoDocumentoVenta = request.TipoDocumentoVenta;
            req.NumeroDocumentoVenta = request.NumeroDocumentoVenta ?? ""; 
            var model = await reporteRepositorio.BuscarOrdenPedido(req);
            return Json(model);
        }

        public async Task<ActionResult> DetalleOrdenPedido()
        {
            string OrdenPedido = "";
            string Modo = "";
            if (!String.IsNullOrEmpty(HttpContext.Request.Query["OrdenPedido"]))
            {
                OrdenPedido = HttpContext.Request.Query["OrdenPedido"];
                Modo = HttpContext.Request.Query["Modo"];
            }
            DetalleOrdenPedidoModel model = new DetalleOrdenPedidoModel();
             
            model.DocumentosIdentidad = await tablasGeneralesRespositorio.ListarTipoDocumentoIdentidad();
            model.DocumentosVenta = await tablasGeneralesRespositorio.ListarTipoDocumentoVenta();
            model.Estados = await tablasGeneralesRespositorio.ListarEstados();
          
            model.FormaEntregas = await tablasGeneralesRespositorio.ListarFormaEntrega();
            model.DetalleOP = reporteRepositorio.ObtenerDetalleOrdenPedido(OrdenPedido);

            var ubigeos = await tablasGeneralesRespositorio.ListarUbigeos();
            model.Departamentos = (from x in ubigeos
                                   where x.CodProvinciaUbigeo == "00"
                                  && x.CodDistritoUbigeo == "00"
                                   select new UbigeoDepartamento() { Codigo = x.CodDepartamentoUbigeo, Nombre = x.NombreUbigeo }).Distinct().ToList();
            model.Provincias = (from x in ubigeos
                                where x.CodDistritoUbigeo == "00"
                                && x.CodDepartamentoUbigeo == model.DetalleOP.OrdenPedido.CodigoUbigeo.Substring(0, 2)
                                select new UbigeoProvincia() { Codigo = x.CodProvinciaUbigeo, Nombre = x.NombreUbigeo }).Distinct().ToList();
            model.Distritos = (from x in ubigeos
                               where x.CodDepartamentoUbigeo == model.DetalleOP.OrdenPedido.CodigoUbigeo.Substring(0, 2)
                               && x.CodProvinciaUbigeo == model.DetalleOP.OrdenPedido.CodigoUbigeo.Substring(2, 2)
                               select new UbigeoDistrito() { Codigo = x.CodDistritoUbigeo, Nombre = x.NombreUbigeo }).Distinct().ToList();
            model.ModoEnable = Modo.ToUpper() ;
            return View(model);
        }

        [HttpPost]
        public async Task<JsonResult> GenerarSolicitud(uspCrearSolTiempoEntrega uspCrearSol)
        {
            DateTime today = DateTime.Now;
            uspCrearSol.LatitudEntrega = "";
            uspCrearSol.LongitudEntrega = "";
            uspCrearSol.FechaEntregaSolTiempoEntrega = today.AddDays(4);
            uspCrearSol.TiempoEntregaSolTiempoEntrega = today.AddHours(12.0).ToString("HH:mm:ss");
            uspCrearSol.ObservacionSolTiempoEntrega = uspCrearSol.ObservacionSolTiempoEntrega ?? "";
            

            if (uspCrearSol.CodigoFormaEntrega == 2 || uspCrearSol.CodigoFormaEntrega == 3)
            {
                string[] LatLng = await ObtenerLatLong(uspCrearSol.DirDestinoSolTiempoEntrega, uspCrearSol.CodigoUbigeo);

                uspCrearSol.LatitudEntrega = LatLng[0];
                uspCrearSol.LongitudEntrega = LatLng[1];
            }
  

            var  oAlmacen = await tablasGeneralesRespositorio.BuscarAlmacen("1");
            string tiempoRutaDestino = ObtenerRutaTiempo(oAlmacen.Latitud, oAlmacen.Longitud, uspCrearSol.LatitudEntrega, uspCrearSol.LongitudEntrega);
            
            var result = await reporteRepositorio.CrearDatosSolicitud(uspCrearSol);
            return Json(result);
        }

        public async Task<string[]> ObtenerLatLong(string direccion, string ubigeo)
        {
            string[] aPtoGeo = new string[2];
            var lUbigeo = await tablasGeneralesRespositorio.BuscarUbigeoTotal(ubigeo);
            lUbigeo.ToList().ForEach(delegate (tbUbigeo oUbigeo) {
                direccion = direccion + " " + oUbigeo.NombreUbigeo;
            }); 
            using (WebClient wc = new WebClient())
            {
                direccion = direccion.Replace(' ', '+');
                string json = wc.DownloadString("https://maps.googleapis.com/maps/api/geocode/json?address=" + direccion + "&key=AIzaSyDk9Uh0YSLIJJl5Cb9VKnNo3cTlV7gmLkU");
                int ptoInicio = json.IndexOf("location") + 13;
                int ptoFin = json.IndexOf("}", ptoInicio) - 1;
                string coordenadas = json.Substring(ptoInicio, ptoFin - ptoInicio).Trim();
                string[] puntosgeo = coordenadas.Split(',');
                aPtoGeo[0] = puntosgeo[0].Split(':')[1];
                aPtoGeo[1] = puntosgeo[1].Split(':')[1];
            }

            return aPtoGeo;
        }

        public string ObtenerRutaTiempo(string LatOrig, string LngOrig, string LatDest, string LngDest)
        {
            using (WebClient wc = new WebClient())
            {
                string json = wc.DownloadString("https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=" + LatOrig + "," + LngOrig + "&destinations=" + LatDest + "," + LngDest + "&key=AIzaSyDk9Uh0YSLIJJl5Cb9VKnNo3cTlV7gmLkU");
                int ptoInicio = json.IndexOf("duration") + 13;
                int ptoFin = json.IndexOf("}", ptoInicio) - 1;
                string tiempo = json.Substring(ptoInicio, ptoFin - ptoInicio).Trim();
                string[] tiempomin = tiempo.Split(',');
                string tiemporeturn = tiempomin[0].Split(':')[1];

                return tiemporeturn.Split(' ')[0];
            }
        }
        
    }
}