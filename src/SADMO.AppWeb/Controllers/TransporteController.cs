﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using SADMO.AppWeb.ViewModels;
using SADMO.Mantenimiento.Infraestructure.Data.Data;

namespace SADMO.AppWeb.Controllers
{
    public class TransporteController : Controller
    {
        private readonly ITranporte tranporteRepositorio;

        public TransporteController(ITranporte tranporteRepositorio)
        {
            this.tranporteRepositorio = tranporteRepositorio;
        }

        public async Task<IActionResult> Monitor()
        {
            
            var  model = await tranporteRepositorio.ListarTransporteOrdenPedido();
            
            
            return View(model);
        }

        public async Task<IActionResult> DetalleTranportista(int Id) {
            var model = new TransporteModel();
            model.RutaEstimada = await tranporteRepositorio.BuscarTransportista(Id);
            model.RutaReal = await tranporteRepositorio.ListarRutasReales();
            var sb = new StringBuilder();
            foreach (var item in model.RutaReal)
            {
                sb.Append($"|{item.LatitudParada},{item.LongitudParada}");
            }
            model.Coordenadas = sb.ToString().Substring(1);
            return View(model);
        }
        [HttpPost]
        private async Task<JsonResult> CalcularCalcularDistanciaDistancia(DetalleTransportistaModel model)
        {
            string urApi = $"https://maps.googleapis.com/maps/api/distancematrix/json?origins={model.LatIni},{model.LongIni}&destinations={model.LatFin},{model.LongFin}&mode=driving&key=AIzaSyCV5nCIY_zdtMBM2S7f6MAO6hcNe4WcpfQ";
            string responseApi;
            using (var httpClient = new HttpClient())
            {
                StringContent content = new StringContent(null, Encoding.UTF8, "application/json");

                using (var response = await httpClient.GetAsync(urApi))
                    responseApi = await response.Content.ReadAsStringAsync();
                return Json(responseApi);
            }
        }
    }

}
