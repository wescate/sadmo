﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using SADMO.AppWeb.ViewModels;
using SADMO.Mantenimiento.Domain;
using SADMO.Mantenimiento.Infraestructure.Data.Interfaces;

namespace SADMO.AppWeb.Controllers
{
    public class RfidController : Controller
    {
        private readonly IRfid rfidRepository;
        private readonly IConfiguration configuration;
        public RfidController(IRfid rfidRepository, IConfiguration configuration)
        {
            this.rfidRepository = rfidRepository;
            this.configuration = configuration;
        }
        public async Task<IActionResult> Index()
        {
            RfidModel model = new RfidModel();
            model.RFIDs = await rfidRepository.ListarRFID();
           
            return View(model);
        }
        public async Task<IActionResult> GrabarMovimiento(string RfidCodInterno, string rfidTag)
        {
            RfidModel model = new RfidModel();
            model.RFIDs = await rfidRepository.ListarRFID();
            string uriApi = configuration.GetValue<string>("urlAPI");
            ApiStockRequest request = new ApiStockRequest();
            request.CodigoInternoRFID = RfidCodInterno;
            request.EtiquetaRFID = rfidTag;
            ApiStockResponse responseModel = new ApiStockResponse();
            string responseApi;
            using (var httpClient = new HttpClient())
            {
                System.Net.Http.StringContent content = new StringContent(JsonConvert.SerializeObject(request), Encoding.UTF8, "application/json");

                using (var response = await httpClient.PostAsync(uriApi, content))
                {
                    responseApi = await response.Content.ReadAsStringAsync();
                    responseModel = JsonConvert.DeserializeObject<ApiStockResponse>(responseApi);
                }
            }
            return View(responseModel);
        }
        
    }
}