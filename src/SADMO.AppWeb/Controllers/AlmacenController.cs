﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SADMO.AppWeb.ViewModels;
using SADMO.Mantenimiento.Infraestructure.Data.Interfaces;

namespace SADMO.AppWeb.Controllers
{
    public class AlmacenController : Controller
    {
        private readonly IProducto productoRepositorio;
        private readonly IAlmacen almacenRepositorio;
        private readonly IStock stockRepoitorio;
        public AlmacenController(IProducto productoRepositorio
                                , IAlmacen almacenRepositorio
                                , IStock stockRepoitorio)
        {
            this.productoRepositorio = productoRepositorio;
            this.almacenRepositorio = almacenRepositorio;
            this.stockRepoitorio = stockRepoitorio;
        }
        public async Task<IActionResult> VerStock()
        {
            StockModel model = new StockModel();
            model.Almacenes = await almacenRepositorio.ObtenerAlmacenes();
            model.Productos = await productoRepositorio.ObtenerProductos();
            model.Movimientos = await stockRepoitorio.MostarStock();
            model.Stocks = (from x in model.Movimientos
                            where x.TipoOperacion == 0
                            select new Models.Stock()
                            {
                                NombreAlmacen = x.NombreAlmacen,
                                DescripcionProducto = x.DescripcionProducto,
                                CantidadStock = x.CantidadStock
                            }).ToList();


            return View(model);
        }
        public async Task<IActionResult> DetalleStock(string almacen,string producto)
        {
            StockDetalleModel model = new StockDetalleModel();
            var detalle = await stockRepoitorio.MostarStock();
            var detalleStocks = (from x in detalle
                                                              where x.NombreAlmacen == almacen &&
                                                              x.DescripcionProducto == producto &&
                                                              x.TipoOperacion != 0
                                                              select new Models.DetalleStock()
                                                              { 
                                                                  TipoOperacion = x.TipoOperacion == 1 ? "Ingreso" : "Salida",
                                                                  CantidadMovimiento = x.CantidadMovimiento,
                                                                  FechaCreacion = x.FechaCreacion,
                                                                  UsuarioCreacion = x.UsuarioCreacion
                                                              }).ToList();
            model.DetalleStock = detalleStocks;
            model.NombreAlmacen = almacen;
            model.DescripcionProducto = producto;
            return View(model);
           
        }

        [HttpGet]
        public async Task<JsonResult> TraerStock(string producto,string almacen) {

            var Movimientos = await stockRepoitorio.MostarStock();

            var model = (from x in Movimientos
                            where x.TipoOperacion == 0
                            && x.NombreAlmacen == almacen
                            && x.DescripcionProducto == producto
                            select new Models.Stock()
                            {
                                NombreAlmacen = x.NombreAlmacen,
                                DescripcionProducto = x.DescripcionProducto,
                                CantidadStock = x.CantidadStock
                            }).ToList(); 

            return Json(model);
        }
    }
}