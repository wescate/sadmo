﻿using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;

namespace MigracionApp
{
    public class MigrationService : IHostedService, IDisposable
    {
        private System.Timers.Timer timer;
        private readonly ILogger<MigrationService> _logger;
        private readonly IDataBaseSADMO _dataSADMO;
        private readonly IDataBaseERP _dataERP;
        public MigrationService(ILogger<MigrationService> logger, IDataBaseSADMO dataSADMO, IDataBaseERP dataERP)
        {
            _logger = logger;
            _dataSADMO = dataSADMO;
            _dataERP = dataERP;
        }

        public void Dispose()
        {
            timer = null;
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            timer = new System.Timers.Timer();
            timer.Interval = Convert.ToInt32(ConfigurationManager.AppSettings["Timer"].ToString());
            Console.WriteLine($"\nIntervalo ed Tiempo  {timer.Interval / 1000} segundos");
            timer.Elapsed += TimerElapsed;
            timer.Start();
          

            return Task.CompletedTask;

        }

        private void TimerElapsed(object sender, ElapsedEventArgs e)
        {
            try
            {

                timer.Stop();
                var dataNueva = _dataERP.ExtraerData();
                Console.WriteLine($"\nExtrayendo Data de ERP");
                if (dataNueva != null && dataNueva.Tables.Count > 0)
                {
                    Console.WriteLine($"Iniciando registro las Ventas");
                    _dataSADMO.RegistrarData(dataNueva.Tables[0], "migracion_tbVenta");
                    Console.WriteLine($"\nFinalizando registro las Ventas");

                    Console.WriteLine($"Iniciando registro de Detalle de Ventas");
                    _dataSADMO.RegistrarData(dataNueva.Tables[1], "migracion_tbdetalleVenta");
                    Console.WriteLine($"\nFinalizando registro de Detalle de Ventas");

                    Console.WriteLine($"Iniciando registro de nuevos productos");
                    _dataSADMO.RegistrarData(dataNueva.Tables[2], "migracion_tbproducto");
                    Console.WriteLine($"\nFInalizando registro de nuevos productos");

                    Console.WriteLine($"Iniciando registro de nuevos clientes");
                    _dataSADMO.RegistrarData(dataNueva.Tables[3], "migracion_tbcliente");
                    Console.WriteLine($"\nFInalizando registro de nuevos clientes");

                    Console.WriteLine($"Iniciando registro de unidades de medida");
                    _dataSADMO.RegistrarData(dataNueva.Tables[4], "migracion_tbUniMed");
                    Console.WriteLine($"\nFInalizando registro de unidades de medida");

                    Console.WriteLine($"Iniciando registro de Tipos de documentos de venta");
                    _dataSADMO.RegistrarData(dataNueva.Tables[5], "migracion_tbTipoDocVenta");
                    Console.WriteLine($"\nFInalizando registro de Tipos de documentos de venta");

                    _dataSADMO.MigrarDataERP_a_SADMO();
                    Console.WriteLine($"\nFinalizando Migración de Datos de ERP ");
                } 
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Ocurrió un error en la ejecucion del servicio. {ex.Message}");
                _logger.LogError(ex, "Ocurrió un error en la ejecucion del servicio");
            }
            finally
            {
                timer.Start();
            }
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            timer.Stop();
            ///_logger.LogInformation("Fin de Servicio");
            return Task.CompletedTask;
        }
    }
}
