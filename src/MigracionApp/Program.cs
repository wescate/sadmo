﻿using ht.sd.orion.ms.ws.queue;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using MigracionApp.Data;
using NLog;
using NLog.Extensions.Logging;
using System;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace MigracionApp
{
    internal class Program
    {
        public static IConfigurationRoot configuration;

        private static async Task Main(string[] args)
        {
            var logger = LogManager.GetCurrentClassLogger();
            var isService = !(Debugger.IsAttached || args.Contains("--console"));
            Console.WriteLine("Iniciando Servicio de Migración");
            var builder = new HostBuilder()
                .ConfigureServices((hostContext, services) =>
                {
                    services.AddHostedService<MigrationService>();
                    services.AddScoped<IDataBaseERP, DA_ERP>();
                    services.AddScoped<IDataBaseSADMO, DA_SADMO>();
                    //services.AddScoped<IGeneralTableRepository, GeneralTableRepository>();

                    services.AddLogging(loggingBuilder =>
                    {

                        loggingBuilder.ClearProviders();
                        loggingBuilder.SetMinimumLevel(Microsoft.Extensions.Logging.LogLevel.Trace);
                        loggingBuilder.AddNLog();
                    });
                });


            if (isService)
            {
                await builder.RunAsServiceAsync();
            }
            else
            {
                await builder.RunConsoleAsync();
            }
        }

    }
}
