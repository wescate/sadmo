﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace MigracionApp
{
    public interface IDataBaseERP
    {
        DataSet ExtraerData();
    }
    public interface IDataBaseSADMO
    {
        void RegistrarData(DataTable dataTable, string nombreTablet);
        void MigrarDataERP_a_SADMO();
    }
}
