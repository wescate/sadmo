﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace MigracionApp.Data
{
    public class DA_SADMO : DA_Base, IDataBaseSADMO
    {
        public void MigrarDataERP_a_SADMO()
        {
            using (SqlConnection connection = new SqlConnection(CXN_SADMO))
            using (SqlCommand command = new SqlCommand("uspIntegracionERP", connection))
            {
                command.CommandType = CommandType.StoredProcedure;
                connection.Open();
                command.ExecuteNonQuery();
                 
            }
        }

        public void RegistrarData(DataTable dataTable, string nombreTable) {
            //DataSet ds = new DataSet();
            using (SqlConnection connection = new SqlConnection(CXN_SADMO))
            using (SqlBulkCopy bulkCopy = new SqlBulkCopy(connection))
            {
                
                connection.Open();
                bulkCopy.DestinationTableName = nombreTable; 
                bulkCopy.WriteToServer(dataTable);
 
            }

        }
    }
}
