﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace MigracionApp.Data
{
    public class DA_ERP : DA_Base,IDataBaseERP
    {
        public DataSet ExtraerData() {
            DataSet ds = new DataSet();
            using (SqlConnection connection = new SqlConnection(CXN_ERP))
            using (SqlCommand command = new SqlCommand("uspExtraeData", connection))
            {
                command.CommandType = CommandType.StoredProcedure;
                connection.Open();
                var da = new SqlDataAdapter(command);
                da.Fill(ds);
            }
            return ds;
        }
    }
}
