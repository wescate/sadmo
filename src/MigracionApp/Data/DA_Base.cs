﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;

namespace MigracionApp.Data
{
    public class DA_Base
    {
        public string CXN_SADMO => ConfigurationManager.ConnectionStrings["ds"].ToString();
        public string CXN_ERP => ConfigurationManager.ConnectionStrings["ds_erp"].ToString();
    }
}
