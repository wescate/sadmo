﻿using Dapper;
using Microsoft.Extensions.Configuration;
using SADMO.Mantenimiento.Domain;
using SADMO.Mantenimiento.Infraestructure.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace SADMO.Mantenimiento.Infraestructure.Data.Data
{
    public class DA_Stock : IStock
    {

        private readonly IConfiguration _configuration;


        protected System.Data.IDbConnection Connection
        {
            get
            {
                return new SqlConnection(_configuration.GetConnectionString("db"));
            }
        }



        public DA_Stock(IConfiguration configuration)
        {
            _configuration = configuration;

        }
        public async Task<ApiStockResponse> ActualizaStock(ApiStockRequest stockRequest)
        {
            using (IDbConnection conn = Connection)
            {
                conn.Open();
                return await conn.QueryFirstAsync<ApiStockResponse>("uspActualizaKardex",
                                                        stockRequest,
                                                        commandType: CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<tbKardex>> MostarStock()
        {
            using (IDbConnection conn = Connection)
            {
                conn.Open();
                return await conn.QueryAsync<tbKardex>("uspObtenerKardex",    
                                                        null,
                                                        commandType: CommandType.StoredProcedure);
            }
        }
    }
}
