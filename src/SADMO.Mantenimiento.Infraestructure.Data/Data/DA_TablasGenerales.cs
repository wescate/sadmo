﻿using Dapper;
using Microsoft.Extensions.Configuration;
using SADMO.Mantenimiento.Domain;
using SADMO.Mantenimiento.Infraestructure.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace SADMO.Mantenimiento.Infraestructure.Data.Data
{
    public class DA_TablasGenerales : ITablasGenerales
    {

        private readonly IConfiguration _configuration;


        protected System.Data.IDbConnection Connection
        {
            get
            {
                return new SqlConnection(_configuration.GetConnectionString("db"));
            }
        }



        public DA_TablasGenerales(IConfiguration configuration)
        {
            _configuration = configuration;

        }
        public async Task<IEnumerable<tbTipoDocumentoIdentidad>> ListarTipoDocumentoIdentidad()
        {
            using (IDbConnection conn = Connection)
            {
                conn.Open();
                return await conn.QueryAsync<tbTipoDocumentoIdentidad>("uspListarTipoDocumento",
                                                        null,
                                                        commandType: CommandType.StoredProcedure);
            }
        }
        
        public async Task<IEnumerable<tbTipoDocVenta>> ListarTipoDocumentoVenta()
        {
            using (IDbConnection conn = Connection)
            {
                conn.Open();
                return await conn.QueryAsync<tbTipoDocVenta>("uspListarTipoDocumentoVenta",
                                                           commandType: CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<tbUbigeo>> ListarUbigeos()
        {
            using (IDbConnection conn = Connection)
            {
                conn.Open();
                return await conn.QueryAsync<tbUbigeo>("uspUbigeo",
                                                           commandType: CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<tbFormaEntrega>> ListarFormaEntrega()
        {
            using (IDbConnection conn = Connection)
            {
                conn.Open();
                return await conn.QueryAsync<tbFormaEntrega>("uspFormaEntrega",
                                                           commandType: CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<tbEstado>> ListarEstados()
        {
            using (IDbConnection conn = Connection)
            {
                conn.Open();
                return await conn.QueryAsync<tbEstado>("uspEstado",
                                                           commandType: CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<tbProducto>> ListarProductos()
        {
            using (IDbConnection conn = Connection)
            {
                conn.Open();
                return await conn.QueryAsync<tbProducto>("uspProducto",
                                                           commandType: CommandType.StoredProcedure);
            }
        }
         

        public async Task<IEnumerable<tbAgenciaTransporte>> ListarAgenciaTransporte()
        {
            using (IDbConnection conn = Connection)
            {
                conn.Open();
                return await conn.QueryAsync<tbAgenciaTransporte>("uspAgenciaTransporte",
                                                           commandType: CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<tbUbigeo>> ListarDetalleUbigeos(int modo, string codigo)
        {
            using (IDbConnection conn = Connection)
            {
                conn.Open();
                return await conn.QueryAsync<tbUbigeo>("uspBucarDetalleUbigeo",
                                                        new { Modo = modo , Codigo = codigo } ,
                                                           commandType: CommandType.StoredProcedure);
            };
        }

        public async Task<IEnumerable<tbUbigeo>> BuscarUbigeoTotal(string codigoUbigeo)
        {
            using (IDbConnection conn = Connection)
            {
                conn.Open();
                return await conn.QueryAsync<tbUbigeo>("uspBuscarUbigeoTotal",
                                                        new { CodigoUbigeo = codigoUbigeo },
                                                           commandType: CommandType.StoredProcedure);
            };
        }

        public async Task<tbAlmacen> BuscarAlmacen(string codigo)
        {
            using (IDbConnection conn = Connection)
            {
                conn.Open();
                return await conn.QueryFirstOrDefaultAsync<tbAlmacen>("uspBuscarAlmacen",
                                                        new { CodigoAlmacen = codigo },
                                                           commandType: CommandType.StoredProcedure);
            };
        }
    }
}
