﻿
using Microsoft.Extensions.Configuration;
using SADMO.Mantenimiento.Domain;
using SADMO.Mantenimiento.Infraestructure.Data.Interfaces;
using System.Linq;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using Dapper;
using System;

namespace SADMO.Mantenimiento.Infraestructure.Data.Data
{
    public class DA_Usuario : IDaUsuario
    {

        private readonly IConfiguration _configuration;
  

        protected System.Data.IDbConnection Connection
        {
            get
            {
                return new SqlConnection(_configuration.GetConnectionString("db"));
            }
        }



        public DA_Usuario(IConfiguration configuration)
        {
            _configuration = configuration;
           
        }


        public async Task<tbUsuarioRegistrado> GetUsuario(string usuario, string password)
        {
            
            using (IDbConnection conn = Connection)
            {
                conn.Open();
                return await conn.QueryFirstAsync<tbUsuarioRegistrado>("uspUsuarioValidarLogin1",
                                                        new { Usuario = usuario, Contrasena = password },
                                                        commandType: CommandType.StoredProcedure);              
            }
          
        }
    }
}
