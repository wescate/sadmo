﻿
using Dapper;
using Microsoft.Extensions.Configuration;
using SADMO.Mantenimiento.Domain;
using SADMO.Mantenimiento.Infraestructure.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace SADMO.Mantenimiento.Infraestructure.Data.Data
{
    public class DA_Producto : IProducto
    {

        private readonly IConfiguration _configuration;


        protected System.Data.IDbConnection Connection
        {
            get
            {
                return new SqlConnection(_configuration.GetConnectionString("db"));
            }
        }

         
        public DA_Producto(IConfiguration configuration)
        {
            _configuration = configuration;

        }
        public async Task<IEnumerable<tbProducto>> ObtenerProductos()
        {
            using (IDbConnection conn = Connection)
            {
                conn.Open();
                return await conn.QueryAsync<tbProducto>("uspObtenerProductos",
                                                        null,
                                                        commandType: CommandType.StoredProcedure);
            }
        }
    }
}
