﻿using Dapper;
using Microsoft.Extensions.Configuration;
using SADMO.Mantenimiento.Domain;
using SADMO.Mantenimiento.Infraestructure.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace SADMO.Mantenimiento.Infraestructure.Data.Data
{
    public class DA_Reporte : IReporte
    {
        private readonly IConfiguration _configuration;


        protected System.Data.IDbConnection Connection
        {
            get
            {
                return new SqlConnection(_configuration.GetConnectionString("db"));
            }
        }



        public DA_Reporte(IConfiguration configuration)
        {
            _configuration = configuration;

        }
        public async Task<IEnumerable<uspDocumentoVenta>> ObtenerDocumentoVenta(BuscarVentaPorCriterio parameters)
        {
            using (IDbConnection conn = Connection)
            {
                conn.Open();
                return await conn.QueryAsync<uspDocumentoVenta>("uspBuscarDocumentoVenta1",
                                                        parameters,
                                                        commandType: CommandType.StoredProcedure);
            }
        }

        public uspSolicitudTiempoEntregaListarResponse ObtenerDetalleVenta(uspSolicitudTiempoEntregaListar parameters)
        {
            uspSolicitudTiempoEntregaListarResponse response = new uspSolicitudTiempoEntregaListarResponse();
            using (IDbConnection conn = Connection)
            {
                conn.Open();
                var gridReader = conn.QueryMultiple("uspSolicitudTiempoEntregaListar",
                                                        parameters,
                                                        commandType: CommandType.StoredProcedure);
                var mockVenta = gridReader.ReadFirst<tbVentaMock>();
                var mockCliente = gridReader.ReadFirst<tbClienteMock>();
                var mockProducto = gridReader.Read<tbProductoMock>();
                response.Venta = mockVenta;
                response.Cliente = mockCliente;
                response.Productos = mockProducto;
            }

            return response;
        }

        public async Task<int> EliminarProductoVenta(uspEliminarProductoVenta parameters)
        {
            using (IDbConnection conn = Connection)
            {
                conn.Open();
                return await conn.ExecuteAsync("uspEliminarProductoVenta",
                                                        parameters,
                                                        commandType: CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<tbProducto>> ListarProductoVenta(string NroVenta)
        {
            using (IDbConnection conn = Connection)
            {
                conn.Open();
                return await conn.QueryAsync<tbProducto>("uspProductosVenta",
                                                        new { Id = NroVenta },
                                                        commandType: CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<uspBucarSolicitudTiempoEntregaResponse>> ListarSolicitudesTE(uspBucarSolicitudTiempoEntrega parameters)
        {
            using (IDbConnection conn = Connection)
            {
                conn.Open();
                return await conn.QueryAsync<uspBucarSolicitudTiempoEntregaResponse>("uspBucarSolicitudTiempoEntrega",
                                                        parameters,
                                                        commandType: CommandType.StoredProcedure);
            }
        }

        public uspDetalleSolicitudEntregaResponse ObtenerDetalleSolicitudTE(string NumeroDocumento)
        {
            uspDetalleSolicitudEntregaResponse response = new uspDetalleSolicitudEntregaResponse();
            using (IDbConnection conn = Connection)
            {
                conn.Open();
                var gridReader = conn.QueryMultiple("uspDetalleSolicitudEntrega",
                                                        new { NRO_SOLICITUD = NumeroDocumento },
                                                        commandType: CommandType.StoredProcedure);
                var mockSolicitudEntrega = gridReader.ReadFirst<MockSolicitudEntrega>();
                var mockProducto = gridReader.Read<MockProductoSolicitud>();
                var Productos = gridReader.Read<tbProducto>();
                response.Solictud = mockSolicitudEntrega;
                response.ProductoSolicitud = mockProducto;
                response.Productos = Productos;
            }

            return response;
        }

        public async Task<int> EliminarProductoSolicitudTiempoEntrega(uspEliminarProductoSolicitudTE parameters)
        {
            using (IDbConnection conn = Connection)
            {
                conn.Open();
                return await conn.ExecuteAsync("uspEliminarProductoSolicitudTE",
                                                        parameters,
                                                        commandType: CommandType.StoredProcedure);
            }
        }

        public async  Task<int> RegistrarProductoSolicitud(IEnumerable<RegistrarProductoSolicitud> parameters)
        {
            using (IDbConnection conn = Connection)
            {
                conn.Open();
                int rowsAffected = 0;
                foreach (var parameter in parameters)
                {
                    rowsAffected += await conn.ExecuteAsync("uspInsertarProductoSolicitud",
                                                       parameter,
                                                       commandType: CommandType.StoredProcedure);
                }
                return rowsAffected;
            }
        }

        public async Task<int> ModificarDatosSolicitud(uspModifcarSolicitud modifcarSolicitud)
        {
            using (IDbConnection conn = Connection)
            {
                conn.Open();
                return await conn.ExecuteAsync("uspModifcarSolicitud",
                                                        modifcarSolicitud,
                                                        commandType: CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<BuscarOrdenPedido>> BuscarOrdenPedido(uspListarOrdenPedido parameters)
        {
            using (IDbConnection conn = Connection)
            {
                conn.Open();
                return await conn.QueryAsync<BuscarOrdenPedido>("uspListarOrdenPedido",
                                                        parameters,
                                                        commandType: CommandType.StoredProcedure);
            }
        }

        public uspDetalleOrdenPedidoResponse ObtenerDetalleOrdenPedido(string ordenPedido)
        {
            uspDetalleOrdenPedidoResponse response = new uspDetalleOrdenPedidoResponse();
            using (IDbConnection conn = Connection)
            {
                conn.Open();
                var gridReader = conn.QueryMultiple("uspDetalleOrdenPedido",
                                                        new { OrdenPedido = ordenPedido },
                                                        commandType: CommandType.StoredProcedure);
                var mockOrdenPedido = gridReader.ReadFirst<DetalleOrdenPedido>();
                var mockProducto = gridReader.Read<ProductoOrdenPedido>();
                
                response.OrdenPedido = mockOrdenPedido;
                response.ProductosOrdenPedido = mockProducto;
            }

            return response;
        }

        public async  Task<int> RegistrarProductoVenta(IEnumerable<RegistrarProductoSolicitud> productoSolicitud)
        {
            using (IDbConnection conn = Connection)
            {
                conn.Open();
                int rowsAffected = 0;
                foreach (var parameter in productoSolicitud)
                {
                    rowsAffected += await conn.ExecuteAsync("uspInsertarProductoVenta",
                                                       parameter,
                                                       commandType: CommandType.StoredProcedure);
                }
                return rowsAffected;
            }
        }

        public async  Task<SolEntregaResponse> CrearDatosSolicitud(uspCrearSolTiempoEntrega uspCrearSol)
        {
            using (IDbConnection conn = Connection)
            {
                conn.Open();
                return await conn.QueryFirstAsync<SolEntregaResponse>("uspCrearSolTiempoEntrega",
                                                        uspCrearSol,
                                                        commandType: CommandType.StoredProcedure);

              
            }
        }

        public async Task<int> ModificarOrdenPedido(uspModifcarOrdenPedido registraSolicitud)
        {
            using (IDbConnection conn = Connection)
            {
                conn.Open();
                return  await conn.ExecuteAsync("uspModifcarOrdenPedido",
                                                        registraSolicitud,
                                                        commandType: CommandType.StoredProcedure);

             
            }
        }
    }
}
