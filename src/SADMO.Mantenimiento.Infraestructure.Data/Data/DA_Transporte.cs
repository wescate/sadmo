﻿using Dapper;
using Microsoft.Extensions.Configuration;
using SADMO.Mantenimiento.Domain;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace SADMO.Mantenimiento.Infraestructure.Data.Data
{
    public class DA_Transporte : ITranporte
    {
        private readonly IConfiguration _configuration;


        protected System.Data.IDbConnection Connection
        {
            get
            {
                return new SqlConnection(_configuration.GetConnectionString("db"));
            }
        }



        public DA_Transporte(IConfiguration configuration)
        {
            _configuration = configuration;

        }
        public async Task<vwTrasnportistaOrdenPedido> BuscarTransportista(int codigoTransportista)
        {
            using (IDbConnection conn = Connection)
            {
                conn.Open();
                return await conn.QueryFirstAsync<vwTrasnportistaOrdenPedido>("uspBuscarTransportista",
                                                        new { CodigoTransportista = codigoTransportista },           
                                                        commandType: CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<vwUnidadTransporteOP>> ListarTransporteOrdenPedido()
        {
            using (IDbConnection conn = Connection)
            {
                conn.Open();
                return await conn.QueryAsync<vwUnidadTransporteOP>("uspListarOPTranporte",
                                                        null,
                                                        commandType: CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<tbRutaReal>> ListarRutasReales()
        {
            using (IDbConnection conn = Connection)
            {
                conn.Open();
                return await conn.QueryAsync<tbRutaReal>("uspRutaReal",
                                                        null,
                                                        commandType: CommandType.StoredProcedure);
            }
        }
    }
}
