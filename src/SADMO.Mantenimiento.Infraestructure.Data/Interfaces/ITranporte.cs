﻿using SADMO.Mantenimiento.Domain;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SADMO.Mantenimiento.Infraestructure.Data.Data
{
    public interface ITranporte
    {
        Task<IEnumerable<vwUnidadTransporteOP>> ListarTransporteOrdenPedido();
        Task<vwTrasnportistaOrdenPedido> BuscarTransportista(int CodigoTransportista);
        Task<IEnumerable<tbRutaReal>> ListarRutasReales();
    }
}
