﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace SADMO.Mantenimiento.Infraestructure.Data.Interfaces
{
    public interface IRepository<T>
    {
        T Add(T entity);
        T Update(T entity);
        T Get(Guid id);
        //T Get(Expression<Func<T, bool>> predicate);
        IEnumerable<T> All();
        IEnumerable<T> Find(Expression<Func<T, bool>> predicate);
        void SaveChanges();
    }
}
