﻿
using SADMO.Mantenimiento.Domain;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SADMO.Mantenimiento.Infraestructure.Data.Interfaces
{
    public interface IRfid
    {
        Task<IEnumerable<uspListarRFID>> ListarRFID();
    }
}
