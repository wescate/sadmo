﻿using SADMO.Mantenimiento.Domain;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SADMO.Mantenimiento.Infraestructure.Data.Interfaces
{
    public interface ITablasGenerales
    {
        Task<IEnumerable<tbTipoDocumentoIdentidad>> ListarTipoDocumentoIdentidad();
        Task<IEnumerable<tbTipoDocVenta>> ListarTipoDocumentoVenta();
        Task<IEnumerable<tbUbigeo>> ListarUbigeos();
        Task<IEnumerable<tbUbigeo>> ListarDetalleUbigeos(int modo, string codigo);
        Task<IEnumerable<tbFormaEntrega>> ListarFormaEntrega();
        Task<IEnumerable<tbEstado>> ListarEstados();
        Task<IEnumerable<tbProducto>> ListarProductos();
        Task<IEnumerable<tbAgenciaTransporte>> ListarAgenciaTransporte();
        Task<IEnumerable<tbUbigeo>> BuscarUbigeoTotal(string codigoUbigeo);
        Task<tbAlmacen> BuscarAlmacen(string codigov);
    }
}
