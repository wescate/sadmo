﻿using SADMO.Mantenimiento.Domain;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SADMO.Mantenimiento.Infraestructure.Data.Interfaces
{
    public interface IStock
    {
        Task<ApiStockResponse> ActualizaStock(ApiStockRequest stockRequest);
        Task<IEnumerable<tbKardex>> MostarStock();

    }
}
