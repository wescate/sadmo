﻿using SADMO.Mantenimiento.Domain;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SADMO.Mantenimiento.Infraestructure.Data.Interfaces
{
     public  interface IAlmacen
    {
        Task<IEnumerable<tbAlmacen>> ObtenerAlmacenes();
    }
    public interface IProducto
    {
        Task<IEnumerable<tbProducto>> ObtenerProductos();
    }
}
