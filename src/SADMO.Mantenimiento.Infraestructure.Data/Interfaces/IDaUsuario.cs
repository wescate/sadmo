﻿ 
using SADMO.Mantenimiento.Domain;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SADMO.Mantenimiento.Infraestructure.Data.Interfaces
{
    public interface IDaUsuario
    {
        Task<tbUsuarioRegistrado> GetUsuario(string usuario, string password);
    }
}
