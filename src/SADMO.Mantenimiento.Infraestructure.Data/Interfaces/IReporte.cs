﻿using SADMO.Mantenimiento.Domain;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SADMO.Mantenimiento.Infraestructure.Data.Interfaces
{
    public interface IReporte
    {
        Task<IEnumerable<uspDocumentoVenta>> ObtenerDocumentoVenta(BuscarVentaPorCriterio parameters);
        uspSolicitudTiempoEntregaListarResponse ObtenerDetalleVenta(uspSolicitudTiempoEntregaListar parameters);
        Task<int> EliminarProductoVenta(uspEliminarProductoVenta parameters);
        Task<IEnumerable<tbProducto>> ListarProductoVenta(string NroVenta);
        Task<IEnumerable<uspBucarSolicitudTiempoEntregaResponse>> ListarSolicitudesTE(uspBucarSolicitudTiempoEntrega parameters);
        uspDetalleSolicitudEntregaResponse ObtenerDetalleSolicitudTE(string NumeroDocumento);
        Task<int> EliminarProductoSolicitudTiempoEntrega(uspEliminarProductoSolicitudTE parameters);
        Task<int> RegistrarProductoSolicitud(IEnumerable<RegistrarProductoSolicitud> parameters);
        Task<int> ModificarDatosSolicitud(uspModifcarSolicitud modifcarSolicitud);
        Task<IEnumerable<BuscarOrdenPedido>> BuscarOrdenPedido(uspListarOrdenPedido req);
        uspDetalleOrdenPedidoResponse ObtenerDetalleOrdenPedido(string ordenPedido);
        Task<int> RegistrarProductoVenta(IEnumerable<RegistrarProductoSolicitud> productoSolicitud);
        Task<SolEntregaResponse> CrearDatosSolicitud(uspCrearSolTiempoEntrega uspCrearSol);
        Task<int> ModificarOrdenPedido(uspModifcarOrdenPedido registraSolicitud);
    }
}
