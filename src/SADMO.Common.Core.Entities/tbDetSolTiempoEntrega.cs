namespace SADMO.Common.Core.Entities
{
    using System;
    using System.Collections.Generic;
    
    public class tbDetSolTiempoEntrega
    {
        public int CodigoSolTiempoEntrega { get; set; }
        public int CodigoProducto { get; set; }
        public decimal CantidadDetSolTiempoEntrega { get; set; }
    }
}
