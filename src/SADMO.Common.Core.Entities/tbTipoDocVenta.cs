namespace SADMO.Common.Core.Entities
{
    using System;
    using System.Collections.Generic;
    
    public class tbTipoDocVenta
    {    
        public string CodigoTipoVenta { get; set; }
        public string DescripcionTipoVenta { get; set; }
    }
}
