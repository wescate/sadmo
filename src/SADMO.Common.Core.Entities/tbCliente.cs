namespace SADMO.Common.Core.Entities
{
    using System;
    using System.Collections.Generic;
    
    public class tbCliente
    {    
        public int CodigoCliente { get; set; }
        public string PrimerNombre { get; set; }
        public string SegundoNombre { get; set; }
        public string ApellidoPaterno { get; set; }
        public string ApellidoMaterno { get; set; }
        public string RazonSocial { get; set; }
        public int CodigoTipoDocIden { get; set; }
        public string NumeroDocIden { get; set; }
        public string Direccion { get; set; }
        public string CodigoUbigeo { get; set; }
        public string Telefono { get; set; }
        public string Correo { get; set; }
    }
}
