namespace SADMO.Common.Core.Entities
{
    using System;
    using System.Collections.Generic;
    
    public class tbStock
    {
        public int CodigoAlmacen { get; set; }
        public int CodigoProducto { get; set; }
        public int TramoProducto { get; set; }
        public decimal CantidadStock { get; set; }
    }
}
