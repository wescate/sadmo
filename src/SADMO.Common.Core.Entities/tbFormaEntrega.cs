namespace SADMO.Common.Core.Entities
{
    using System;
    using System.Collections.Generic;
    
    public class tbFormaEntrega
    {    
        public int CodigoFormaEntrega { get; set; }
        public string DescripcionFormaEntrega { get; set; }
    }
}
