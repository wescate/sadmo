namespace SADMO.Common.Core.Entities
{
    using System;
    using System.Collections.Generic;
    
    public class tbVenta
    {
        public string CodigoTipoVenta { get; set; }
        public string NumeroDocVenta { get; set; }
        public int CodigoCliente { get; set; }
        public DateTime FechaEmision { get; set; }
    }
}
