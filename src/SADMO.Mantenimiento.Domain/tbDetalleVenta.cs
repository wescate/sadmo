namespace SADMO.Mantenimiento.Domain
{
    using System;
    using System.Collections.Generic;
    
    public class tbDetalleVenta
    {
        public string CodigoTipoVenta { get; set; }
        public string NumeroDocVenta { get; set; }
        public int CodigoCliente { get; set; }
        public int CodigoProducto { get; set; }
        public decimal CantidadDetVenta { get; set; }
    }
}
