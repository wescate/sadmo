﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SADMO.Mantenimiento.Domain
{
    public class BuscarVentaPorCriterio
    {
        public BuscarVentaPorCriterio()
        {
            Cliente = ""; 
            NumeroDocumento = ""; 
            NumeroDocumentoVenta = "";
            FechaEmisionInicio = "";
            FechaEmisionFin = "";
        }
        public string Cliente { get; set; }
        public int TipoDocumento { get; set; }
        public string NumeroDocumento { get; set; }
        public string TipoDocumentoVenta { get; set; }
        public string NumeroDocumentoVenta { get; set; }
        public string FechaEmisionInicio { get; set; }
        public string FechaEmisionFin { get; set; }
    }
}
