﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SADMO.Mantenimiento.Domain
{
    public class BuscarOrdenPedido
    {
        public string NumeroDocumento { get; set; }
        public string Estado { get; set; }
        public string RazonSocial { get; set; }
    }
}
