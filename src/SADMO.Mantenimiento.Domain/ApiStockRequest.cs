﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SADMO.Mantenimiento.Domain
{
    public class ApiStockRequest
    {
        public string EtiquetaRFID { get; set; }
        public string CodigoInternoRFID { get; set; }
    
        public string UsuarioCreacion => "UserRFID";
    }
}
