namespace SADMO.Mantenimiento.Domain
{
    using System;
    using System.Collections.Generic;
    
    public class tbUbigeo
    {    
        public string CodigoUbigeo { get; set; }
        public string CodDepartamentoUbigeo { get; set; }
        public string CodProvinciaUbigeo { get; set; }
        public string CodDistritoUbigeo { get; set; }
        public string NombreUbigeo { get; set; }
    }
}
