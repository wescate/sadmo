﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SADMO.Mantenimiento.Domain
{
    public class vwUnidadTransporteOP
    {
        public string NumeroDocumento { get; set; }
        public string NombreTransporte { get; set; }
        public string Placa { get; set; }
        public string DescripcionTipoRuta { get; set; }
        public int CodigoTransportista { get; set; } 
        public decimal Latitud { get; set; }
        public decimal Longitud { get; set; }
        public string RazonSocial { get; set; }
    }
}
