namespace SADMO.Mantenimiento.Domain
{
    using System;
    using System.Collections.Generic;
    
    public class tbEstado
    {    
        public int CodigoEstado { get; set; }
        public string DescripcionEstado { get; set; }
    }
}
