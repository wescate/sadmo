﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SADMO.Mantenimiento.Domain
{
    public class uspDetalleOrdenPedidoResponse
    {
        public DetalleOrdenPedido OrdenPedido  { get; set; }
        public IEnumerable<ProductoOrdenPedido> ProductosOrdenPedido { get; set; }
    }

    public class ProductoOrdenPedido
    {
        public int CodigoProducto { get; set; }
        public decimal Cantidad { get; set; }
        public string CodigoUniMed { get; set; }
        public string DescripcionProducto { get; set; }
        public string MarcaProducto { get; set; }
        public decimal Tramo { get; set; } 
    }
}
