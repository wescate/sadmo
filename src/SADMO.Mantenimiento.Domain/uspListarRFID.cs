﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SADMO.Mantenimiento.Domain
{
    public class uspListarRFID
    {
        public string CodigoInterno { get; set; }
        public string TipoOperacion { get; set; }
        public string Ubicacion { get; set; }
        public string NombreAlmacen { get; set; }
    }
}
