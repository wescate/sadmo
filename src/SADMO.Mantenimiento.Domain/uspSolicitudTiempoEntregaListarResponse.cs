﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SADMO.Mantenimiento.Domain
{
    public class uspSolicitudTiempoEntregaListarResponse
    {
        public tbVentaMock Venta { get; set; }
        public tbClienteMock Cliente { get; set; }
        public IEnumerable<tbProductoMock> Productos { get; set; }
    }

    public class tbVentaMock
    {
        public int CodigoSolTiempoEntrega { get; set; }
        public string CodigoTipoVenta { get; set; }
        public string NumeroDocVenta { get; set; }
        public int CodigoCliente { get; set; }
        public DateTime FechaEmisionSolTiempoEntrega { get; set; }
        public string DirDestinoSolTiempoEntrega { get; set; }
        public string CodigoUbigeo { get; set; }
        public int CodigoFormaEntrega { get; set; }
        public int tbEstado_CodigoEstado { get; set; }
        public DateTime FechaEntregaSolTiempoEntrega { get; set; }
        public string TiempoEntregaSolTiempoEntrega { get; set; }
        public string ObservacionSolTiempoEntrega { get; set; } 
    }
    public class tbClienteMock {
        public string RazonSocial { get; set; }
        public int CodigoTipoDocIden { get; set; }
        public string NumeroDocIden { get; set; }
    }
    public class tbProductoMock
    {
        public int CodigoProducto { get; set; }
        public decimal CantidadDetSolTiempoEntrega { get; set; }
        public string CodigoUniMed { get; set; }
        public string DescripcionProducto { get; set; }
        public string MarcaProducto { get; set; } 
    }
}
