﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SADMO.Mantenimiento.Domain
{
    public class vwTrasnportistaOrdenPedido
    {
        public string NombreTransportista { get; set; }
        public string NombreTransporte { get; set; }
        public string RazonSocial { get; set; }
        public string DescripcionFormaEntrega { get; set; }
        public decimal Latitud { get; set; }
        public decimal Longitud { get; set; }

    }
}
