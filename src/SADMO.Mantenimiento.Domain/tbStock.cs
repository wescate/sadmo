namespace SADMO.Mantenimiento.Domain
{
    using System;
    using System.Collections.Generic;
    
    public class tbKardex
    {
        public int CodigoStock { get; set; }
        public string NombreAlmacen { get; set; }
        public string DescripcionProducto { get; set; }
        public int TipoOperacion { get; set; }
        public decimal? CantidadMovimiento { get; set; }
        public decimal? CantidadStock { get; set; }
        public bool Habilitado { get; set; }
        public string Observaciones { get; set; }
        public DateTime FechaCreacion { get; set; }
        public string UsuarioCreacion { get; set; }
        public DateTime FechaModificacion { get; set; }
        public string UsuarioModificacion { get; set; }
    }
}
