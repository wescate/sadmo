﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SADMO.Mantenimiento.Domain
{
    public class uspModifcarSolicitud
    {
        public int Estado { get; set; }
        public int FormaEntrega { get; set; }
        public string CodigoUbigeo { get; set; }
        public int NumeroSolicitud { get; set; }
        public string DireccionEntrega { get; set; }
        public int AgenciaTransporte { get; set; }
    }
}
