﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SADMO.Mantenimiento.Domain
{
    public class uspBucarSolicitudTiempoEntregaResponse
    {
        public string CodigoSolTiempoEntrega { get; set; }
        public string DescripcionEstado { get; set; }
        public string DescripcionFormaEntrega { get; set; }
        public string RazonSocial { get; set; }
    }
}
