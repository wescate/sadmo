namespace SADMO.Mantenimiento.Domain
{
    using System;
    using System.Collections.Generic;
    
    public class tbSolTiempoEntrega
    {
        public int CodigoSolTiempoEntrega { get; set; }
        public string CodigoTipoVenta { get; set; }
        public string NumeroDocVenta { get; set; }
        public int CodigoCliente { get; set; }
        public DateTime FechaEmisionSolTiempoEntrega { get; set; }
        public string DirDestinoSolTiempoEntrega { get; set; }
        public string CodigoUbigeo { get; set; }
        public int CodigoFormaEntrega { get; set; }
        public int CodigoAgenciaTransporte { get; set; }
        public int tbEstado_CodigoEstado { get; set; }
        public DateTime FechaEntregaSolTiempoEntrega { get; set; }
        public string TiempoEntregaSolTiempoEntrega { get; set; }
        public string ObservacionSolTiempoEntrega { get; set; }
        public string LatitudEntrega { get; set; }
        public string LongitudEntrega { get; set; }
    }
    public class uspCrearSolTiempoEntrega
    {
        //public int CodigoSolTiempoEntrega { get; set; }
        public string CodigoTipoVenta { get; set; }
        public string NumeroDocVenta { get; set; }
        //public int CodigoCliente { get; set; }
        //public DateTime FechaEmisionSolTiempoEntrega { get; set; }
        public string DirDestinoSolTiempoEntrega { get; set; }
        public string CodigoUbigeo { get; set; }
        public int CodigoFormaEntrega { get; set; }
        public int CodigoAgenciaTransporte { get; set; }
        public int tbEstado_CodigoEstado { get; set; }
        public DateTime FechaEntregaSolTiempoEntrega { get; set; }
        public string TiempoEntregaSolTiempoEntrega { get; set; }
        public string ObservacionSolTiempoEntrega { get; set; }
        public string LatitudEntrega { get; set; }
        public string LongitudEntrega { get; set; }
    }
}
