﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SADMO.Mantenimiento.Domain
{
    public class tbAgenciaTransporte
    {
        public int CodigoAgenciaTransporte { get; set; }
        public string NombreAgencia { get; set; }
        public string Direccion { get; set; }
        public string CodigoUbigeo { get; set; }
        public string horaInicioAtencion { get; set; }
        public string HoraFinAtencion { get; set; }
    }
}
