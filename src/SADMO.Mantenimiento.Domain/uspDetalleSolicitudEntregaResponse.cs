﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SADMO.Mantenimiento.Domain
{
    public class uspDetalleSolicitudEntregaResponse
    {
        public MockSolicitudEntrega Solictud { get; set; }
        public IEnumerable<MockProductoSolicitud> ProductoSolicitud { get; set; }
        public IEnumerable<tbProducto> Productos { get; set; }
    }

    public class MockSolicitudEntrega
    {
        public string NroDocumento { get; set; }
        public string FechaEmisionSolTiempoEntrega { get; set; }
        public int CodigoEstado { get; set; }
        public string RazonSocial { get; set; }
        public string DirDestinoSolTiempoEntrega { get; set; }
        public string DescripcionDocIden { get; set; }
        public string CodigoTipoVenta { get; set; }
        public string NumeroDocVenta { get; set; }
        public string NumeroDocIden { get; set; }
        public string CodigoUbigeo { get; set; }
        public string ObservacionSolTiempoEntrega { get; set; }
        public string TiempoEntregaSolTiempoEntrega { get; set; }
        public int CodigoFormaEntrega { get; set; }
        public int CodigoAgenciaTransporte { get; set; }
        
    }

    public class MockProductoSolicitud
    {
        public int CodigoProducto { get; set; }
        public decimal CantidadDetSolTiempoEntrega { get; set; }
        public string CodigoUniMed { get; set; }
        public string DescripcionProducto { get; set; }
        public string MarcaProducto { get; set; }

    }
}

