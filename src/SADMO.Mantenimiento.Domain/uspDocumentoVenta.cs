﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SADMO.Mantenimiento.Domain
{
    public class uspDocumentoVenta
    {
        public string DescripcionTipoVenta { get; set; }
        public string NumeroDocVenta { get; set; }
        public string RazonSocial { get; set; }
        public string FechaEmision { get; set; }
    }
}
