﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SADMO.Mantenimiento.Domain
{
    public class uspModifcarOrdenPedido
    {
        public int CodigoEstado { get; set; }
        public int CodigoOrdenPedido { get; set; }
    }
}
