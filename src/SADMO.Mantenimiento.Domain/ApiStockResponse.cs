﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SADMO.Mantenimiento.Domain
{
    public class ApiStockResponse
    {
        public bool Estado { get; set; }
        public string Mensaje { get; set; }
    }
}
