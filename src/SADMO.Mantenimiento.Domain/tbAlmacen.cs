namespace SADMO.Mantenimiento.Domain
{
    using System;
    using System.Collections.Generic;

    public class tbAlmacen
    {
        public int CodigoAlmacen { get; set; }
        public string NombreAlmacen { get; set; }
        public string DireccionAlmacen { get; set; }
        public string CodigoUbigeo { get; set; }
        public string Latitud { get; set; }
        public string Longitud { get; set; }
    }
}
