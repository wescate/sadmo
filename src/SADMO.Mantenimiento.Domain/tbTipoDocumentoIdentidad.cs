namespace SADMO.Mantenimiento.Domain
{
    using System;
    using System.Collections.Generic;
    
    public class tbTipoDocumentoIdentidad
    {    
        public int CodigoTipoDocIden { get; set; }
        public string DescripcionDocIden { get; set; }
    }
}
