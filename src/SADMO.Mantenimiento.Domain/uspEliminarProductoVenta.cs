﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SADMO.Mantenimiento.Domain
{
    public class uspEliminarProductoVenta
    {
        public string NumeroDocVenta { get; set; }
        public int CodigoProducto { get; set; }
    }
}
