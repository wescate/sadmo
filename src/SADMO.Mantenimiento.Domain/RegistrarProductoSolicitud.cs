﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SADMO.Mantenimiento.Domain
{
    public class RegistrarProductoSolicitud
    {
        public int idProducto { get; set; }
        public decimal Cantidad { get; set; }
        public string NumeroSolicitud { get; set; }
    }
}
