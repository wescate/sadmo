﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SADMO.Mantenimiento.Domain
{
    public class SolEntregaResponse
    {
        public string CodigoSolicitud { get; set; }
        public string FechaEntregaEstimada { get; set; }
    }
}
