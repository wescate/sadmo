namespace SADMO.Mantenimiento.Domain
{
    using System;
    using System.Collections.Generic;
    
    public class tbUniMed
    {    
        public string CodigoUniMed { get; set; }
        public string DescripcionUniMed { get; set; }
    }
}
