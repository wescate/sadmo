﻿using System;

namespace SADMO.Mantenimiento.Domain
{
    public class DetalleOrdenPedido
    {
        public string CodigoOrdenPedido { get; set; }
        public string FechaEmision { get; set; }
        public string CodigoSolTiempoEntrega { get; set; }
        public int CodigoEstado { get; set; }
        public string RazonSocial { get; set; }
        public string DirDestinoSolTiempoEntrega { get; set; }
        public string DescripcionDocIden { get; set; }
        public string CodigoTipoVenta { get; set; }
        public string NumeroDocVenta { get; set; }
        public string NumeroDocIden { get; set; } 
        
        public string CodigoUbigeo { get; set; }
        public string Observacion { get; set; }
        public string TiempoEntregaSolTiempoEntrega { get; set; }
        public DateTime? TiempoEntregaReal { get; set; }
        public int CodigoFormaEntrega { get; set; }
    }
}