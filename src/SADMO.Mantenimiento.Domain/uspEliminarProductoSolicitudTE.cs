﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SADMO.Mantenimiento.Domain
{
    public class uspEliminarProductoSolicitudTE
    {
        public string NumeroSolicitud { get; set; }
        public int CodigoProducto { get; set; }
    }
}
