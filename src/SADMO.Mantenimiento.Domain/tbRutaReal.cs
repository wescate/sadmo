﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SADMO.Mantenimiento.Domain
{
   public  class tbRutaReal
    {
        public int CodigoRutaReal { get; set; }
        public int CodigoTransporte { get; set; }
        public decimal LatitudParada { get; set; }
        public decimal LongitudParada { get; set; }
    }
}
