﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SADMO.Mantenimiento.Domain
{
    public class tbUsuarioRegistrado
    {
        public int CodigoUsuario { get; set; }
        public string NombreCompleto { get; set; }
    }
}
